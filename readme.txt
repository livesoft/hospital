安装步骤
1, 执行create_table.sql
	(默认创建数据库hospital, 用户hospital密码hospital)
2, 修改src/main/resources/config.properties, 改为本地的配置
	db.url			连接串
	db.username		默认 hospital
	db.password		默认 hospital
	
已初始化5个用户
用户名	角色
010001	院办
010002	人事部门
010003	科长（业务科室）
010004	直接领导
010005	员工
010006	科长（职能科室）
密码和用户名一样
	
运行环境
Java8
Tomcat8

使用技术
Spring4

JPA(Hibernate实现)	持久层 数据库访问
Spring MVC			web框架

Spring Security		登录和权限

Maven3				项目管理
Spring Test			单元测试

JSP					页面渲染
Tiles3				JSP布局管理
JQuery
Bootstrap			前端css,javascript