package com.bondqin.demo.test.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bondqin.demo.dao.AnnualPlanDao;
import com.bondqin.demo.entity.AnnualPlan;

@Rollback(value=true)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:applicationContext.xml"})
public class AnnualPlanDaoTest {
	
	protected Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AnnualPlanDao annualPlanDao;
	
	@PersistenceContext
	private EntityManager em;

	@Test
	public void testFindPage() {
		Pageable pageable = new PageRequest(0, 5);

		Page<AnnualPlan> pageResult = annualPlanDao.findAll(pageable);
		System.out.println("totalElements:" + pageResult.getTotalElements() + ", totalPage:" + pageResult.getTotalPages());
		
		for (AnnualPlan entity : pageResult.getContent()) {
			System.out.println(entity.getPlanID() + "-" + entity.getPlanName());
		}

	}
}