package com.bondqin.demo.test.dao;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bondqin.demo.dao.UserDao;
import com.bondqin.demo.entity.Authority;
import com.bondqin.demo.entity.User;
import com.bondqin.demo.util.PrincipalUtil;

@Rollback(value=false)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:applicationContext.xml"})
public class UserDaoTest {
	
	protected Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserDao userDao;

	/**
	 * 添加用户
	 */
	@Test
	@Transactional
	public void testAddUser() {
		String username = "010006";
		String password = "010006";
		String[] roleNames = new String[] { "ROLE_R3" };
		
		if (!userDao.exists(username)) {
			User user = new User();
			user.setUsername(username);
			user.setEnabled(1);
			user.setPassword(PrincipalUtil.encodePassword(password));
			
			user.setAuthorities(new ArrayList<Authority>());
			
			for (String roleName : roleNames) {
				Authority auth = new Authority();
				auth.setAuthority(roleName);
				user.addAuthority(auth);
			}
			
			userDao.save(user);
		}
	}
	
	/**
	 * 删除用户
	 */
	@Test
	@Transactional
	public void testDeleteUser() {
		String username = "test";
		
		if (userDao.exists(username)) {
			userDao.delete(username);
		}
	}

}