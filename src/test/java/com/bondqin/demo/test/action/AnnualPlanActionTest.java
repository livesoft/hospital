package com.bondqin.demo.test.action;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bondqin.demo.action.AnnualPlanAction;
import com.bondqin.demo.vo.AnnualPlanVo;
import com.bondqin.demo.vo.ResultModel;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author bond
 *
 */
@Rollback(value=true)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:applicationContext.xml"})
public class AnnualPlanActionTest {
	
	protected Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AnnualPlanAction annualPlanAction;

	@Test
	public void testListAll() throws Exception {
		
		Map<String, Object> result = annualPlanAction.listAll(0, 10);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		log.info(objectMapper.writeValueAsString(result));
	}
	
	@Test
	public void testAdd() throws Exception {
		AnnualPlanVo record = new AnnualPlanVo();
		record.setAddedContent("2");
		record.setCultureBuildGoal("2");
		record.setFinanceGoal("2");
		record.setHardwareBuildGoal("2");
		record.setManageGoal("2");
		record.setPlanMeasure(2016);
		record.setPlanName("2");
		record.setProjectGoal("2");
		record.setTalentBuildGoal("2");
		record.setTransactionGoal("2");
		
		ResultModel result = annualPlanAction.add(record);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		log.info(objectMapper.writeValueAsString(result));
	}
	
	@Test
	public void testModify() throws Exception {
		AnnualPlanVo record = new AnnualPlanVo();
		record.setPlanID(1);
		record.setAddedContent("5");
		record.setCultureBuildGoal("5");
		record.setFinanceGoal("5");
		record.setHardwareBuildGoal("5");
		record.setManageGoal("5");
		record.setPlanMeasure(2016);
		record.setPlanName("5");
		record.setProjectGoal("5");
		record.setTalentBuildGoal("5");
		record.setTransactionGoal("5");
		
		ResultModel result = annualPlanAction.modify(record);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		log.info(objectMapper.writeValueAsString(result));
	}
	
	@Test
	public void testRemove() throws Exception {
		ResultModel result = annualPlanAction.remove(1);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		log.info(objectMapper.writeValueAsString(result));
	}
}