package com.bondqin.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bondqin.demo.dao.ConductMethodDao;
import com.bondqin.demo.entity.ConductAdoptKpi;
import com.bondqin.demo.entity.ConductMethod;
import com.bondqin.demo.entity.KpiIndex;

@Service
public class MethodConductService {
	
	@Autowired
	private ConductMethodDao conductMethodDao;
	
	/**
	 * 根据季度，部门类型查询指标
	 * @param applyDate
	 * @param deptType
	 * @return
	 */
	public List<KpiIndex> queryByApplyDate(Integer applyDate, Integer deptType) {
		List<ConductMethod> list = conductMethodDao.findByConductApplyDate(applyDate);

		List<KpiIndex> result = new ArrayList<KpiIndex>();
		if (list.size() > 0) {
			ConductMethod conductMethod = list.get(0);
			
			List<ConductAdoptKpi> adoptKpiList = conductMethod.getConductAdoptKpis();
			for (ConductAdoptKpi adoptKpi : adoptKpiList) {
				if (ObjectUtils.equals(adoptKpi.getType(), deptType)) {
					result.add(adoptKpi.getKpiIndex());
				}
			}
		}
		return result;
	}
	
}
