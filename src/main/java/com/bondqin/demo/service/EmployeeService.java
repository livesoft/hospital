package com.bondqin.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bondqin.demo.dao.ConductMethodDao;
import com.bondqin.demo.dao.PerformanceAssessmentDao;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.entity.KpiIndex;
import com.bondqin.demo.vo.KpiIndexScoreVo;

@Service
public class EmployeeService {
	
	@Autowired
	private ConductMethodDao conductMethodDao;
	
	@Autowired
	private PerformanceAssessmentDao performanceAssessmentDao;
	
	/**
	 * 查询总得分
	 * @param employee
	 * @return
	 */
	//TODO
	public Integer queryTotalScore(Integer applyDate, Employee employee) {
		return RandomUtils.nextInt(100);
	}
	
	/**
	 * 查询指标得分
	 * @param index
	 * @return
	 */
	//TODO
	public List<KpiIndexScoreVo> queryIndexScore(Integer applyDate, List<KpiIndex> kpiList) {
		
		List<KpiIndexScoreVo> voList = new ArrayList<KpiIndexScoreVo>();
		
		for (KpiIndex kpi : kpiList) {
			KpiIndexScoreVo vo = new KpiIndexScoreVo();
			
			BeanUtils.copyProperties(kpi, vo);
			
			vo.setScore(RandomUtils.nextInt(100));
			voList.add(vo);
		}
		
		return voList;
	}
	
}
