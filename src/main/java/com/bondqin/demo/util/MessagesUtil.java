package com.bondqin.demo.util;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * MessageSource工具类
 * @author bond
 *
 */
public class MessagesUtil {

	private static ResourceBundleMessageSource messages;
	
	public void setMessages(ResourceBundleMessageSource messageSource) {
		messages = messageSource;
	}
	
	public static String getMessage(String code, String defaultMessage, Object[] args) {
		Locale locale = LocaleContextHolder.getLocale();

		return messages.getMessage(code, args, defaultMessage, locale);
	}
	
	public static String getMessage(String code, Object... args) {
		Locale locale = LocaleContextHolder.getLocale();

		return messages.getMessage(code, args, locale);
	}
	
	public static String getMessage(String code) {
		Locale locale = LocaleContextHolder.getLocale();

		return messages.getMessage(code, null, locale);
	}
	
	/**
	 * 解析code，按顺序找知道找到存在的消息
	 * @param codes
	 * @return
	 */
	public static String getMessage(String[] codes, String defaultMessage) {
		String msg = null;
		if (codes == null) {
			return null;
		}
		for (String code : codes) {
			try {
				msg = getMessage(code);
			} catch(Exception e) {
				continue;
			}
			break;
		}
		if (msg == null) {
			return defaultMessage;
		}
		return msg;
	}
}
