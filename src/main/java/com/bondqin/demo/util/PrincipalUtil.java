package com.bondqin.demo.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.bondqin.demo.config.RoleEnum;

/**
 * 鉴权工具类
 * @author bond
 *
 */
public class PrincipalUtil {

	/**
	 * 获取登录用户名
	 * @return
	 */
	public static String getUsername() {
		SecurityContext context = SecurityContextHolder.getContext();
		
		Authentication auth = context.getAuthentication();
		if (auth == null) {
			return null;
		}
		
		Object principal = auth.getPrincipal();

		String username = null;
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		} else {
			username = principal.toString();
		}
		
		return username;
	}
	
	/**
	 * 获权限文本
	 * @return
	 */
	public static String getRoleText() {
		SecurityContext context = SecurityContextHolder.getContext();
		
		Authentication auth = context.getAuthentication();
		if (auth == null) {
			return null;
		}
		
		List<String> result = new ArrayList<String>();
		Collection<?> list = auth.getAuthorities();
		for(Object obj : list) {
			result.add(RoleEnum.getText(String.valueOf(obj)));
		}
		
		return StringUtils.join(result, ",");
	}

	/**
	 * 密码编码
	 * @param password
	 * @return
	 */
	public static String encodePassword(String password) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(password);
	}
	
	public static void main(String[] args) {
		for (String arg : args) {
			String str = encodePassword(arg);
			
			System.out.println(str);
		}
	}
}
