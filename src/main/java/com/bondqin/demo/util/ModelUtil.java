package com.bondqin.demo.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.util.Assert;

/**
 * 模型工具类
 * @author bond
 *
 */
public class ModelUtil {

	/**
	 * 实体类转vo
	 * @param entity		实体对象
	 * @param modelType		模型类
	 * @return
	 */
	public static <E, M> M toModel(E entity, Class<M> modelType) {
		Assert.notNull(entity);
		
		M model = null;
		try {
			model =	modelType.newInstance();
			//简单属性拷贝
			BeanUtils.copyProperties(model, entity);
		} catch (Exception e) {
		}
		
		return model;
	}
	
	/**
	 * 实体类转vo
	 * @param entity		实体对象
	 * @param modelType		模型类
	 * @param converter		转换器
	 * @return
	 */
	public static <E, M> M toModel(E entity, Class<M> modelType, Converter<E, M> converter) {
		Assert.notNull(entity);
		Assert.notNull(modelType);
		Assert.notNull(converter);
		
		M model = converter.convert(entity);

		return model;
	}
	
	/**
	 * 实体列表转模型列表
	 * @param entityList	实体对象列表
	 * @param modelType		模型类
	 * @param converter		转换器
	 * @return
	 */
	public static <E, M> List<M> toList(List<E> entityList, Class<M> modelType, Converter<E, M> converter) {
		Assert.notNull(entityList);
		Assert.notNull(modelType);
		Assert.notNull(converter);
		
		List<M> modelList = new ArrayList<M>();
		
		for(E entity : entityList) {
			M model = converter.convert(entity);
			modelList.add(model);
		}

		return modelList;
	}
	
	/**
	 * 实体列表转模型列表
	 * @param entityList	实体对象列表
	 * @param modelType		模型类
	 * @return
	 */
	public static <E, M> List<M> toList(List<E> entityList, Class<M> modelType) {
		Assert.notNull(entityList);
		Assert.notNull(modelType);
		
		List<M> modelList = new ArrayList<M>();
		
		for(E entity : entityList) {
			M model = null;
			try {
				model =	modelType.newInstance();
				//简单属性拷贝
				BeanUtils.copyProperties(model, entity);
			} catch (Exception e) {
			}
			modelList.add(model);
		}
		return modelList;
	}
	
	/**
	 * 实体分页对象转模型分页对象
	 * @param entityPage	 实体分页对象
	 * @param modelType		模型分页对象
	 * @return
	 */
	public static <E, M> Page<M> toPage(Page<E> entityPage, Class<M> modelType) {
		Assert.notNull(entityPage);
		Assert.notNull(modelType);
		
		Page<M> pageM = entityPage.map(new Converter<E, M>() {

			@Override
			public M convert(E source) {
				M model = null;
				try {
					model =	modelType.newInstance();
					//简单属性拷贝
					BeanUtils.copyProperties(model, source);
				} catch (Exception e) {
				}
				
				return model;
			}
			
		});
		
		return pageM;
	}
	
	/**
	 * 对象之间拷贝属性，忽略null值
	 * @param source	源对象
	 * @param target	目标对象
	 */
	public static void copyPropertiesNotNull(Object source, Object target) {
		Assert.notNull(source);
		Assert.notNull(target);

		//查找所有Model属性字段
		Class<?> modelClazz = source.getClass();
		Field[] fields = modelClazz.getDeclaredFields();
		
		for (Field field : fields) {
			String name = field.getName();
			
			try {
				Object obj = PropertyUtils.getProperty(source, name);
				if (obj != null) {
					BeanUtils.setProperty(source, name, obj);
				}
			} catch (Exception e) {
			}
		}
	}
	
	public static Map<String, Object> toDatatable(Page<?> pageVo) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("recordsTotal", pageVo.getTotalElements());
		result.put("recordsFiltered", pageVo.getTotalElements());
		result.put("data", pageVo.getContent());
		return result;
	}
	
	public static Map<String, Object> emptyDatatable() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("recordsTotal", 0);
		result.put("recordsFiltered", 0);
		result.put("data", new ArrayList<Object>());
		return result;
	}
}
