package com.bondqin.demo.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具类
 * 
 */
public class DateUtil {
	
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	
	public static final String DATETIME_PATTERN = "yyyy-MM-dd hh:mm:ss";

	/**
	 * 日期转字符串
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);

		return format.format(date);
    }

	/**
	 * 字符串转日期
	 * @param str
	 * @return
	 */
	public static Date parse(String str) {
		if (str == null) {
			return null;
		}
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);;
        try {
            return format.parse(str);
        } catch (ParseException ex) {
            return null;
        }
    }

}
