package com.bondqin.demo.vo;

/**
 * @author bond
 *
 */
public class TaskVo {
	
	private Integer id;

	private Integer applyDate;

	private Integer directorID;

	private Integer employeeID;

	private Integer state;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Integer applyDate) {
		this.applyDate = applyDate;
	}

	public Integer getDirectorID() {
		return directorID;
	}

	public void setDirectorID(Integer directorID) {
		this.directorID = directorID;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
	
}
