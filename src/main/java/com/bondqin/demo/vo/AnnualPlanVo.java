package com.bondqin.demo.vo;
/**
 * 医院年度计划Vo
 * @author bond
 *
 */
public class AnnualPlanVo {
	/**
	 * 计划编号
	 */
	private Integer planID;
	/**
	 * 补充内容
	 */
	private String addedContent;
	/**
	 * 文化建设目标
	 */
	private String cultureBuildGoal;
	/**
	 * 财务目标
	 */
	private String financeGoal;
	/**
	 * 硬件建设目标
	 */
	private String hardwareBuildGoal;
	/**
	 * 管理目标
	 */
	private String manageGoal;
	/**
	 * 计划适用年份
	 */
	private Integer planMeasure;
	/**
	 * 计划名称
	 */
	private String planName;
	/**
	 * 科研目标
	 */
	private String projectGoal;
	/**
	 * 人才建设目标
	 */
	private String talentBuildGoal;
	/**
	 * 业务目标
	 */
	private String transactionGoal;
	
	public Integer getPlanID() {
		return planID;
	}
	public void setPlanID(Integer planID) {
		this.planID = planID;
	}
	public String getAddedContent() {
		return addedContent;
	}
	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}
	public String getCultureBuildGoal() {
		return cultureBuildGoal;
	}
	public void setCultureBuildGoal(String cultureBuildGoal) {
		this.cultureBuildGoal = cultureBuildGoal;
	}
	public String getFinanceGoal() {
		return financeGoal;
	}
	public void setFinanceGoal(String financeGoal) {
		this.financeGoal = financeGoal;
	}
	public String getHardwareBuildGoal() {
		return hardwareBuildGoal;
	}
	public void setHardwareBuildGoal(String hardwareBuildGoal) {
		this.hardwareBuildGoal = hardwareBuildGoal;
	}
	public String getManageGoal() {
		return manageGoal;
	}
	public void setManageGoal(String manageGoal) {
		this.manageGoal = manageGoal;
	}
	public Integer getPlanMeasure() {
		return planMeasure;
	}
	public void setPlanMeasure(Integer planMeasure) {
		this.planMeasure = planMeasure;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getProjectGoal() {
		return projectGoal;
	}
	public void setProjectGoal(String projectGoal) {
		this.projectGoal = projectGoal;
	}
	public String getTalentBuildGoal() {
		return talentBuildGoal;
	}
	public void setTalentBuildGoal(String talentBuildGoal) {
		this.talentBuildGoal = talentBuildGoal;
	}
	public String getTransactionGoal() {
		return transactionGoal;
	}
	public void setTransactionGoal(String transactionGoal) {
		this.transactionGoal = transactionGoal;
	}

}
