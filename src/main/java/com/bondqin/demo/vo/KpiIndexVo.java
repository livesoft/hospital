package com.bondqin.demo.vo;
/**
 * @author bond
 *
 */
public class KpiIndexVo {
	
	private Integer id;

	private Integer indexCategoryID;

	private String indexCategoryName;

	private Integer indexComparativeWeight;

	private String indexName;

	private String indexRemark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndexCategoryID() {
		return indexCategoryID;
	}

	public void setIndexCategoryID(Integer indexCategoryID) {
		this.indexCategoryID = indexCategoryID;
	}

	public String getIndexCategoryName() {
		return indexCategoryName;
	}

	public void setIndexCategoryName(String indexCategoryName) {
		this.indexCategoryName = indexCategoryName;
	}

	public Integer getIndexComparativeWeight() {
		return indexComparativeWeight;
	}

	public void setIndexComparativeWeight(Integer indexComparativeWeight) {
		this.indexComparativeWeight = indexComparativeWeight;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getIndexRemark() {
		return indexRemark;
	}

	public void setIndexRemark(String indexRemark) {
		this.indexRemark = indexRemark;
	}
	
}
