package com.bondqin.demo.vo;

import java.util.List;


/**
 * The persistent class for the conduct_method database table.
 * 
 */
public class ConductMethodVo {

	private Integer id;

	private Integer employeeAssessmentRate;

	private String addedContent;

	private Integer conductApplyDate;

	private String conductName;

	private Integer managerAssessmentRate;

	private Integer rankMethodID;

	private List<ConductAdoptKpiVo> serviceKpis;
	
	private List<ConductAdoptKpiVo> functionKpis;

	public ConductMethodVo() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmployeeAssessmentRate() {
		return employeeAssessmentRate;
	}

	public void setEmployeeAssessmentRate(Integer employeeAssessmentRate) {
		this.employeeAssessmentRate = employeeAssessmentRate;
	}

	public String getAddedContent() {
		return addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public Integer getConductApplyDate() {
		return conductApplyDate;
	}

	public void setConductApplyDate(Integer conductApplyDate) {
		this.conductApplyDate = conductApplyDate;
	}

	public String getConductName() {
		return conductName;
	}

	public void setConductName(String conductName) {
		this.conductName = conductName;
	}

	public Integer getManagerAssessmentRate() {
		return managerAssessmentRate;
	}

	public void setManagerAssessmentRate(Integer managerAssessmentRate) {
		this.managerAssessmentRate = managerAssessmentRate;
	}

	public Integer getRankMethodID() {
		return rankMethodID;
	}

	public void setRankMethodID(Integer rankMethodID) {
		this.rankMethodID = rankMethodID;
	}

	public List<ConductAdoptKpiVo> getServiceKpis() {
		return serviceKpis;
	}

	public void setServiceKpis(List<ConductAdoptKpiVo> serviceKpis) {
		this.serviceKpis = serviceKpis;
	}

	public List<ConductAdoptKpiVo> getFunctionKpis() {
		return functionKpis;
	}

	public void setFunctionKpis(List<ConductAdoptKpiVo> functionKpis) {
		this.functionKpis = functionKpis;
	}

	
}
