package com.bondqin.demo.vo;

import java.util.List;

/**
 * @author bond
 *
 */
public class PerformanceAssessmentVo {
	
	private Integer id;
	
	private Integer assessmentApplyDate;
	
	private String directLeaderAssessmentContent;
	
	private Integer employeeID;
	
	private Integer scoreSource;
	
	private List<KpiPerformanceAssessmentVo> kpiAssessList;
	
	private Integer scoreEmployeeID;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDirectLeaderAssessmentContent() {
		return directLeaderAssessmentContent;
	}

	public void setDirectLeaderAssessmentContent(String directLeaderAssessmentContent) {
		this.directLeaderAssessmentContent = directLeaderAssessmentContent;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public Integer getScoreSource() {
		return scoreSource;
	}

	public void setScoreSource(Integer scoreSource) {
		this.scoreSource = scoreSource;
	}

	public List<KpiPerformanceAssessmentVo> getKpiAssessList() {
		return kpiAssessList;
	}

	public void setKpiAssessList(List<KpiPerformanceAssessmentVo> kpiAssessList) {
		this.kpiAssessList = kpiAssessList;
	}

	public Integer getScoreEmployeeID() {
		return scoreEmployeeID;
	}

	public void setScoreEmployeeID(Integer scoreEmployeeID) {
		this.scoreEmployeeID = scoreEmployeeID;
	}

	public Integer getAssessmentApplyDate() {
		return assessmentApplyDate;
	}

	public void setAssessmentApplyDate(Integer assessmentApplyDate) {
		this.assessmentApplyDate = assessmentApplyDate;
	}
	
}
