package com.bondqin.demo.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

/**
 * 结果模型类
 * @author bond
 *
 */
public class ResultModel {

	/**
	 * 错误信息
	 */
	private List<String> errors;
	
	/**
	 * 属性-错误 Map
	 */
	private Map<String, String> errorMap;
	
	/**
	 * 存储结果返回值
	 */
	private Map<String, Object> content = new HashMap<String, Object>();

	public boolean hasErrors() {
		return CollectionUtils.isNotEmpty(errors);
	}

	public List<String> getErrors() {
		return errors;
	}
	
	public Map<String, String> getErrorMap() {
		return errorMap;
	}
	
	public void putError(String key, String error) {
		if (errorMap == null) {
			errorMap = new HashMap<String, String>();
		}
		errorMap.put(key, error);
		addError(error);
	}

	public void addError(String error) {
		if (errors == null) {
			errors = new ArrayList<String>();
		}
		errors.add(error);
	}
	
	public void put(String key, Object value) {
		content.put(key, value);
	}
	
	public Map<String, Object> toMap() {
		content.put("hasErrors", hasErrors());
		if (MapUtils.isNotEmpty(errorMap)) {
			content.put("errorMap", getErrorMap());
		}
		if (CollectionUtils.isNotEmpty(errors)) {
			content.put("errors", getErrors());
		}
		
		return content;
	}

}