package com.bondqin.demo.vo;

import java.util.Date;

import com.bondqin.demo.config.ReviewReferenceTypeEnum;
import com.bondqin.demo.util.DateUtil;

/**
 * @author bond
 *
 */
public class ReviewReferenceVo {
	private Integer id;

	private String referenceContent;

	private Date referenceDate;

	private String referenceName;

	private Integer referenceType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReferenceContent() {
		return referenceContent;
	}

	public void setReferenceContent(String referenceContent) {
		this.referenceContent = referenceContent;
	}

	public Date getReferenceDate() {
		return referenceDate;
	}
	
	public String getReferenceDateText() {
		return DateUtil.format(referenceDate);
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public String getReferenceName() {
		return referenceName;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	public Integer getReferenceType() {
		return referenceType;
	}

	public void setReferenceType(Integer referenceType) {
		this.referenceType = referenceType;
	}
	
	public String getReferenceTypeText() {
		return ReviewReferenceTypeEnum.getText(referenceType);
	}
}
