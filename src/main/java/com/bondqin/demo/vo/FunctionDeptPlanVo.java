package com.bondqin.demo.vo;

import java.util.Date;

import com.bondqin.demo.util.DateUtil;

/**
 * @author bond
 *
 */
public class FunctionDeptPlanVo {
	private Integer id;

	private String addedContent;

	private String cultureBuildGoal;

	private String manageGoal;

	private String planName;

	private Date planStartDate;

	private Date planStopDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddedContent() {
		return addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public String getCultureBuildGoal() {
		return cultureBuildGoal;
	}

	public void setCultureBuildGoal(String cultureBuildGoal) {
		this.cultureBuildGoal = cultureBuildGoal;
	}

	public String getManageGoal() {
		return manageGoal;
	}

	public void setManageGoal(String manageGoal) {
		this.manageGoal = manageGoal;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getPlanStartDate() {
		return planStartDate;
	}
	
	public String getPlanStartDateText() {
		return DateUtil.format(planStartDate);
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanStopDate() {
		return planStopDate;
	}
	
	public String getPlanStopDateText() {
		return DateUtil.format(planStopDate);
	}

	public void setPlanStopDate(Date planStopDate) {
		this.planStopDate = planStopDate;
	}

	
}
