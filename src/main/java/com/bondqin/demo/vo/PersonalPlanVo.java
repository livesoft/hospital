package com.bondqin.demo.vo;

import java.util.Date;

import com.bondqin.demo.util.DateUtil;

/**
 * @author bond
 *
 */
public class PersonalPlanVo {
	private Integer id;

	private String addedContent;

	private String planName;

	private Date planStartDate;

	private Date planStopDate;

	private String projectGoal;

	private String transactionGoal;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddedContent() {
		return addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getPlanStartDate() {
		return planStartDate;
	}
	
	public String getPlanStartDateText() {
		return DateUtil.format(planStartDate);
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanStopDate() {
		return planStopDate;
	}
	
	public String getPlanStopDateText() {
		return DateUtil.format(planStopDate);
	}

	public void setPlanStopDate(Date planStopDate) {
		this.planStopDate = planStopDate;
	}

	public String getProjectGoal() {
		return projectGoal;
	}

	public void setProjectGoal(String projectGoal) {
		this.projectGoal = projectGoal;
	}

	public String getTransactionGoal() {
		return transactionGoal;
	}

	public void setTransactionGoal(String transactionGoal) {
		this.transactionGoal = transactionGoal;
	}

}
