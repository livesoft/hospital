package com.bondqin.demo.vo;

/**
 * @author bond
 *
 */
public class KpiPerformanceAssessmentVo {
	
	private Integer id;

	private Integer indexScore;

	private Integer kpiIndex;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndexScore() {
		return indexScore;
	}

	public void setIndexScore(Integer indexScore) {
		this.indexScore = indexScore;
	}

	public Integer getKpiIndex() {
		return kpiIndex;
	}

	public void setKpiIndex(Integer kpiIndex) {
		this.kpiIndex = kpiIndex;
	}
	
}
