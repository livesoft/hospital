package com.bondqin.demo.vo;
/**
 * @author bond
 *
 */
public class ConductAdoptKpiVo {
	
private Integer id;
	
	private Integer indexCategoryWeight;

	private Integer type;

	private Integer kpiIndex;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndexCategoryWeight() {
		return indexCategoryWeight;
	}

	public void setIndexCategoryWeight(Integer indexCategoryWeight) {
		this.indexCategoryWeight = indexCategoryWeight;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getKpiIndex() {
		return kpiIndex;
	}

	public void setKpiIndex(Integer kpiIndex) {
		this.kpiIndex = kpiIndex;
	}

}
