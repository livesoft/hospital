package com.bondqin.demo.vo;

import java.util.Date;

import com.bondqin.demo.util.DateUtil;

/**
 * @author bond
 *
 */
public class ServiceDeptPlanVo {
	/**
	 * 计划编号
	 */
	private Integer id;
	/**
	 * 补充内容
	 */
	private String addedContent;
	/**
	 * 文化建设目标
	 */
	private String cultureBuildGoal;
	/**
	 * 财务目标
	 */
	private String financeGoal;
	/**
	 * 硬件建设目标
	 */
	private String hardwareBuildGoal;
	/**
	 * 计划编号
	 */
	private String planName;

	/**
	 * 计划起始日期
	 */
	private Date planStartDate;

	/**
	 * 计划结束日期
	 */
	private Date planStopDate;
	/**
	 * 科研目标
	 */
	private String projectGoal;
	/**
	 * 人才建设目标
	 */
	private String talentBuildGoal;
	/**
	 * 业务目标
	 */
	private String transactionGoal;
	
	/**
	 * 部门ID
	 */
	private Integer departmentID;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAddedContent() {
		return addedContent;
	}
	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}
	public String getCultureBuildGoal() {
		return cultureBuildGoal;
	}
	public void setCultureBuildGoal(String cultureBuildGoal) {
		this.cultureBuildGoal = cultureBuildGoal;
	}
	public String getFinanceGoal() {
		return financeGoal;
	}
	public void setFinanceGoal(String financeGoal) {
		this.financeGoal = financeGoal;
	}
	public String getHardwareBuildGoal() {
		return hardwareBuildGoal;
	}
	public void setHardwareBuildGoal(String hardwareBuildGoal) {
		this.hardwareBuildGoal = hardwareBuildGoal;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Date getPlanStartDate() {
		return planStartDate;
	}
	
	public String getPlanStartDateText() {
		return DateUtil.format(planStartDate);
	}
	
	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}
	
	public String getPlanStopDateText() {
		return DateUtil.format(planStopDate);
	}
	
	public Date getPlanStopDate() {
		return planStopDate;
	}
	public void setPlanStopDate(Date planStopDate) {
		this.planStopDate = planStopDate;
	}
	public String getProjectGoal() {
		return projectGoal;
	}
	public void setProjectGoal(String projectGoal) {
		this.projectGoal = projectGoal;
	}
	public String getTalentBuildGoal() {
		return talentBuildGoal;
	}
	public void setTalentBuildGoal(String talentBuildGoal) {
		this.talentBuildGoal = talentBuildGoal;
	}
	public String getTransactionGoal() {
		return transactionGoal;
	}
	public void setTransactionGoal(String transactionGoal) {
		this.transactionGoal = transactionGoal;
	}
	public Integer getDepartmentID() {
		return departmentID;
	}
	public void setDepartmentID(Integer departmentID) {
		this.departmentID = departmentID;
	}

}
