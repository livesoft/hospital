package com.bondqin.demo.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the conduct_adopt_kpi database table.
 * 
 */
@Entity
@Table(name="conduct_adopt_kpi")
@NamedQuery(name="ConductAdoptKpi.findAll", query="SELECT c FROM ConductAdoptKpi c")
public class ConductAdoptKpi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Integer indexCategoryWeight;

	private Integer type;

	//bi-directional many-to-one association to ConductMethod
	@ManyToOne
	@JoinColumn(name="conduct_method_id")
	private ConductMethod conductMethod;

	//uni-directional one-to-one association to KpiIndex
	@OneToOne
	@JoinColumn(name="indexCategoryID")
	private KpiIndex kpiIndex;

	public ConductAdoptKpi() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndexCategoryWeight() {
		return this.indexCategoryWeight;
	}

	public void setIndexCategoryWeight(Integer indexCategoryWeight) {
		this.indexCategoryWeight = indexCategoryWeight;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public ConductMethod getConductMethod() {
		return this.conductMethod;
	}

	public void setConductMethod(ConductMethod conductMethod) {
		this.conductMethod = conductMethod;
	}

	public KpiIndex getKpiIndex() {
		return this.kpiIndex;
	}

	public void setKpiIndex(KpiIndex kpiIndex) {
		this.kpiIndex = kpiIndex;
	}

}