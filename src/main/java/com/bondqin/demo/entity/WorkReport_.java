package com.bondqin.demo.entity;

import javax.persistence.metamodel.SingularAttribute;


public class WorkReport_ {
	public static volatile SingularAttribute<WorkReport, Integer> id;
	public static volatile SingularAttribute<WorkReport, String> reportContent;
	public static volatile SingularAttribute<WorkReport, Integer> reportDate;
	public static volatile SingularAttribute<WorkReport, String> reportName;
	public static volatile SingularAttribute<WorkReport, Employee> employee;
}
