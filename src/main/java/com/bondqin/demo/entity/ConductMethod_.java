package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-08T18:56:11.619+0800")
@StaticMetamodel(ConductMethod.class)
public class ConductMethod_ {
	public static volatile SingularAttribute<ConductMethod, Integer> id;
	public static volatile SingularAttribute<ConductMethod, Integer> employeeAssessmentRate;
	public static volatile SingularAttribute<ConductMethod, String> addedContent;
	public static volatile SingularAttribute<ConductMethod, Integer> conductApplyDate;
	public static volatile SingularAttribute<ConductMethod, String> conductName;
	public static volatile SingularAttribute<ConductMethod, Integer> managerAssessmentRate;
	public static volatile SingularAttribute<ConductMethod, Integer> rankMethodID;
	public static volatile ListAttribute<ConductMethod, ConductAdoptKpi> conductAdoptKpis;
}
