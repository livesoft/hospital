package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the personal_plan database table.
 * 
 */
@Entity
@Table(name="personal_plan")
@NamedQuery(name="PersonalPlan.findAll", query="SELECT p FROM PersonalPlan p")
public class PersonalPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String addedContent;

	private String planName;

	@Temporal(TemporalType.DATE)
	private Date planStartDate;

	@Temporal(TemporalType.DATE)
	private Date planStopDate;

	private String projectGoal;

	private String transactionGoal;

	//uni-directional one-to-one association to Employee
	@OneToOne
	@JoinColumn(name="employeeID")
	private Employee employee;
	
	@Column(updatable=false,insertable=false)
	private Integer employeeID;

	public PersonalPlan() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddedContent() {
		return this.addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getPlanStartDate() {
		return this.planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanStopDate() {
		return this.planStopDate;
	}

	public void setPlanStopDate(Date planStopDate) {
		this.planStopDate = planStopDate;
	}

	public String getProjectGoal() {
		return this.projectGoal;
	}

	public void setProjectGoal(String projectGoal) {
		this.projectGoal = projectGoal;
	}

	public String getTransactionGoal() {
		return this.transactionGoal;
	}

	public void setTransactionGoal(String transactionGoal) {
		this.transactionGoal = transactionGoal;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

}