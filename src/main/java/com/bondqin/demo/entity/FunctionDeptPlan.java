package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the function_dept_plan database table.
 * 
 */
@Entity
@Table(name="function_dept_plan")
@NamedQuery(name="FunctionDeptPlan.findAll", query="SELECT f FROM FunctionDeptPlan f")
public class FunctionDeptPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String addedContent;

	private String cultureBuildGoal;

	private String manageGoal;

	private String planName;

	@Temporal(TemporalType.DATE)
	private Date planStartDate;

	@Temporal(TemporalType.DATE)
	private Date planStopDate;

	//uni-directional one-to-one association to Department
	@OneToOne
	@JoinColumn(name="departmentID")
	private Department department;
	
	@Column(updatable=false,insertable=false)
	private Integer departmentID;

	public FunctionDeptPlan() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddedContent() {
		return this.addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public String getCultureBuildGoal() {
		return this.cultureBuildGoal;
	}

	public void setCultureBuildGoal(String cultureBuildGoal) {
		this.cultureBuildGoal = cultureBuildGoal;
	}

	public String getManageGoal() {
		return this.manageGoal;
	}

	public void setManageGoal(String manageGoal) {
		this.manageGoal = manageGoal;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getPlanStartDate() {
		return this.planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanStopDate() {
		return this.planStopDate;
	}

	public void setPlanStopDate(Date planStopDate) {
		this.planStopDate = planStopDate;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Integer getDepartmentID() {
		return departmentID;
	}

	public void setDepartmentID(Integer departmentID) {
		this.departmentID = departmentID;
	}
	
}