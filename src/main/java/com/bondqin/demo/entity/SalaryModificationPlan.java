package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the salary_modification_plan database table.
 * 
 */
@Entity
@Table(name="salary_modification_plan")
@NamedQuery(name="SalaryModificationPlan.findAll", query="SELECT s FROM SalaryModificationPlan s")
public class SalaryModificationPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Integer modificationApplyDate;

	private Integer modificationPercent;

	//uni-directional one-to-one association to Employee
	@OneToOne
	@JoinColumn(name="employeeID")
	private Employee employee;

	public SalaryModificationPlan() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getModificationApplyDate() {
		return this.modificationApplyDate;
	}

	public void setModificationApplyDate(Integer modificationApplyDate) {
		this.modificationApplyDate = modificationApplyDate;
	}

	public Integer getModificationPercent() {
		return this.modificationPercent;
	}

	public void setModificationPercent(Integer modificationPercent) {
		this.modificationPercent = modificationPercent;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}