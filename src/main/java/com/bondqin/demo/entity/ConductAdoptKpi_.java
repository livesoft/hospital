package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-01T22:30:46.264+0800")
@StaticMetamodel(ConductAdoptKpi.class)
public class ConductAdoptKpi_ {
	public static volatile SingularAttribute<ConductAdoptKpi, Integer> id;
	public static volatile SingularAttribute<ConductAdoptKpi, Integer> indexCategoryWeight;
	public static volatile SingularAttribute<ConductAdoptKpi, Integer> type;
	public static volatile SingularAttribute<ConductAdoptKpi, ConductMethod> conductMethod;
	public static volatile SingularAttribute<ConductAdoptKpi, KpiIndex> kpiIndex;
}
