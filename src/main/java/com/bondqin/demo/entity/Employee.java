package com.bondqin.demo.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


/**
 * The persistent class for the employee database table.
 * 
 */
@Entity
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String employeeGender;

	private String employeeName;

	private Double employeeSalary;

	private String workNo;

	//uni-directional one-to-one association to Position
	@OneToOne
	@NotFound(action=NotFoundAction.IGNORE)
	@JoinColumn(name="employeePosition")
	private Position position;

	//uni-directional one-to-one association to Employee
	@OneToOne
	@NotFound(action=NotFoundAction.IGNORE)
	@JoinColumn(name="directLeader")
	private Employee directLeader;
	
	//uni-directional one-to-one association to Department
	@OneToOne
	@JoinColumn(name="employeeDepartment")
	@NotFound(action=NotFoundAction.IGNORE)
	private Department department;

	public Employee() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmployeeGender() {
		return this.employeeGender;
	}

	public void setEmployeeGender(String employeeGender) {
		this.employeeGender = employeeGender;
	}

	public String getEmployeeName() {
		return this.employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Double getEmployeeSalary() {
		return this.employeeSalary;
	}

	public void setEmployeeSalary(Double employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public String getWorkNo() {
		return this.workNo;
	}

	public void setWorkNo(String workNo) {
		this.workNo = workNo;
	}

	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Employee getDirectLeader() {
		return this.directLeader;
	}

	public void setDirectLeader(Employee directLeader) {
		this.directLeader = directLeader;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}