package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T22:25:13.721+0800")
@StaticMetamodel(Authority.class)
public class Authority_ {
	public static volatile SingularAttribute<Authority, Integer> id;
	public static volatile SingularAttribute<Authority, String> authority;
	public static volatile SingularAttribute<Authority, User> user;
}
