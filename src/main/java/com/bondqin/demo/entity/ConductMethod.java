package com.bondqin.demo.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the conduct_method database table.
 * 
 */
@Entity
@Table(name="conduct_method")
@NamedQuery(name="ConductMethod.findAll", query="SELECT c FROM ConductMethod c")
public class ConductMethod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="360AssessmentRate")
	private Integer employeeAssessmentRate;

	private String addedContent;

	private Integer conductApplyDate;

	private String conductName;

	private Integer managerAssessmentRate;

	private Integer rankMethodID;

	//bi-directional many-to-one association to ConductAdoptKpi
	@OneToMany(mappedBy="conductMethod",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<ConductAdoptKpi> conductAdoptKpis;

	public ConductMethod() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmployeeAssessmentRate() {
		return employeeAssessmentRate;
	}

	public void setEmployeeAssessmentRate(Integer employeeAssessmentRate) {
		this.employeeAssessmentRate = employeeAssessmentRate;
	}

	public String getAddedContent() {
		return this.addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public Integer getConductApplyDate() {
		return this.conductApplyDate;
	}

	public void setConductApplyDate(Integer conductApplyDate) {
		this.conductApplyDate = conductApplyDate;
	}

	public String getConductName() {
		return this.conductName;
	}

	public void setConductName(String conductName) {
		this.conductName = conductName;
	}

	public Integer getManagerAssessmentRate() {
		return this.managerAssessmentRate;
	}

	public void setManagerAssessmentRate(Integer managerAssessmentRate) {
		this.managerAssessmentRate = managerAssessmentRate;
	}

	public Integer getRankMethodID() {
		return this.rankMethodID;
	}

	public void setRankMethodID(Integer rankMethodID) {
		this.rankMethodID = rankMethodID;
	}

	public List<ConductAdoptKpi> getConductAdoptKpis() {
		return this.conductAdoptKpis;
	}

	public void setConductAdoptKpis(List<ConductAdoptKpi> conductAdoptKpis) {
		this.conductAdoptKpis = conductAdoptKpis;
	}

	public ConductAdoptKpi addConductAdoptKpi(ConductAdoptKpi conductAdoptKpi) {
		getConductAdoptKpis().add(conductAdoptKpi);
		conductAdoptKpi.setConductMethod(this);

		return conductAdoptKpi;
	}

	public ConductAdoptKpi removeConductAdoptKpi(ConductAdoptKpi conductAdoptKpi) {
		getConductAdoptKpis().remove(conductAdoptKpi);
		conductAdoptKpi.setConductMethod(null);

		return conductAdoptKpi;
	}

}