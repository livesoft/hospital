package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-29T15:55:17.556+0800")
@StaticMetamodel(Position.class)
public class Position_ {
	public static volatile SingularAttribute<Position, Integer> id;
	public static volatile SingularAttribute<Position, String> positionContent;
	public static volatile SingularAttribute<Position, Integer> positionDepartment;
	public static volatile SingularAttribute<Position, String> positionFunction;
	public static volatile SingularAttribute<Position, String> positionName;
	public static volatile SingularAttribute<Position, Integer> positionSalary;
}
