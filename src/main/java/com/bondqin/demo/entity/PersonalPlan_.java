package com.bondqin.demo.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-29T23:57:23.673+0800")
@StaticMetamodel(PersonalPlan.class)
public class PersonalPlan_ {
	public static volatile SingularAttribute<PersonalPlan, Integer> id;
	public static volatile SingularAttribute<PersonalPlan, String> addedContent;
	public static volatile SingularAttribute<PersonalPlan, String> planName;
	public static volatile SingularAttribute<PersonalPlan, Date> planStartDate;
	public static volatile SingularAttribute<PersonalPlan, Date> planStopDate;
	public static volatile SingularAttribute<PersonalPlan, String> projectGoal;
	public static volatile SingularAttribute<PersonalPlan, String> transactionGoal;
	public static volatile SingularAttribute<PersonalPlan, Employee> employee;
	public static volatile SingularAttribute<PersonalPlan, Integer> employeeID;
}
