package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-08T18:56:11.749+0800")
@StaticMetamodel(PerformanceAssessment.class)
public class PerformanceAssessment_ {
	public static volatile SingularAttribute<PerformanceAssessment, Integer> id;
	public static volatile SingularAttribute<PerformanceAssessment, String> directLeaderAssessmentContent;
	public static volatile SingularAttribute<PerformanceAssessment, Integer> employeeID;
	public static volatile SingularAttribute<PerformanceAssessment, Integer> assessmentApplyDate;
	public static volatile SingularAttribute<PerformanceAssessment, Integer> scoreSource;
	public static volatile ListAttribute<PerformanceAssessment, KpiPerformanceAssessment> kpiPerformanceAssessments;
	public static volatile SingularAttribute<PerformanceAssessment, Integer> scoreEmployeeID;
}
