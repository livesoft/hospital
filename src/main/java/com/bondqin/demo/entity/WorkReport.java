package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the work_report database table.
 * 
 */
@Entity
@Table(name="work_report")
@NamedQuery(name="WorkReport.findAll", query="SELECT w FROM WorkReport w")
public class WorkReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String reportContent;

	private Integer reportDate;

	private String reportName;
	
	@Column(updatable=false, insertable=false)
	private Integer employeeID;

	//uni-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="employeeID")
	private Employee employee;

	public WorkReport() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReportContent() {
		return this.reportContent;
	}

	public void setReportContent(String reportContent) {
		this.reportContent = reportContent;
	}

	public Integer getReportDate() {
		return this.reportDate;
	}

	public void setReportDate(Integer reportDate) {
		this.reportDate = reportDate;
	}

	public String getReportName() {
		return this.reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

}