package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-25T23:24:47.614+0800")
@StaticMetamodel(AnnualPlan.class)
public class AnnualPlan_ {
	public static volatile SingularAttribute<AnnualPlan, Integer> planID;
	public static volatile SingularAttribute<AnnualPlan, String> addedContent;
	public static volatile SingularAttribute<AnnualPlan, String> cultureBuildGoal;
	public static volatile SingularAttribute<AnnualPlan, String> financeGoal;
	public static volatile SingularAttribute<AnnualPlan, String> hardwareBuildGoal;
	public static volatile SingularAttribute<AnnualPlan, String> manageGoal;
	public static volatile SingularAttribute<AnnualPlan, Integer> planMeasure;
	public static volatile SingularAttribute<AnnualPlan, String> planName;
	public static volatile SingularAttribute<AnnualPlan, String> projectGoal;
	public static volatile SingularAttribute<AnnualPlan, String> talentBuildGoal;
	public static volatile SingularAttribute<AnnualPlan, String> transactionGoal;
}
