package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the performance_assessment database table.
 * 
 */
@Entity
@Table(name="performance_assessment")
@NamedQuery(name="PerformanceAssessment.findAll", query="SELECT p FROM PerformanceAssessment p")
public class PerformanceAssessment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String directLeaderAssessmentContent;

	private Integer employeeID;

	private Integer assessmentApplyDate;
	
	private Integer scoreSource;

	//bi-directional many-to-one association to KpiPerformanceAssessment
	@OneToMany(mappedBy="performanceAssessment",cascade=CascadeType.ALL)
	private List<KpiPerformanceAssessment> kpiPerformanceAssessments;

	private Integer scoreEmployeeID;

	public PerformanceAssessment() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDirectLeaderAssessmentContent() {
		return this.directLeaderAssessmentContent;
	}

	public void setDirectLeaderAssessmentContent(String directLeaderAssessmentContent) {
		this.directLeaderAssessmentContent = directLeaderAssessmentContent;
	}

	public Integer getEmployeeID() {
		return this.employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public Integer getScoreSource() {
		return this.scoreSource;
	}

	public void setScoreSource(Integer scoreSource) {
		this.scoreSource = scoreSource;
	}

	public List<KpiPerformanceAssessment> getKpiPerformanceAssessments() {
		return this.kpiPerformanceAssessments;
	}

	public void setKpiPerformanceAssessments(List<KpiPerformanceAssessment> kpiPerformanceAssessments) {
		this.kpiPerformanceAssessments = kpiPerformanceAssessments;
	}

	public KpiPerformanceAssessment addKpiPerformanceAssessment(KpiPerformanceAssessment kpiPerformanceAssessment) {
		getKpiPerformanceAssessments().add(kpiPerformanceAssessment);
		kpiPerformanceAssessment.setPerformanceAssessment(this);

		return kpiPerformanceAssessment;
	}

	public KpiPerformanceAssessment removeKpiPerformanceAssessment(KpiPerformanceAssessment kpiPerformanceAssessment) {
		getKpiPerformanceAssessments().remove(kpiPerformanceAssessment);
		kpiPerformanceAssessment.setPerformanceAssessment(null);

		return kpiPerformanceAssessment;
	}

	public Integer getScoreEmployeeID() {
		return scoreEmployeeID;
	}

	public void setScoreEmployeeID(Integer scoreEmployeeID) {
		this.scoreEmployeeID = scoreEmployeeID;
	}

	public Integer getAssessmentApplyDate() {
		return assessmentApplyDate;
	}

	public void setAssessmentApplyDate(Integer assessmentApplyDate) {
		this.assessmentApplyDate = assessmentApplyDate;
	}

	
}