package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-29T15:55:17.551+0800")
@StaticMetamodel(Department.class)
public class Department_ {
	public static volatile SingularAttribute<Department, Integer> id;
	public static volatile SingularAttribute<Department, String> departmentFunction;
	public static volatile SingularAttribute<Department, String> departmentName;
	public static volatile SingularAttribute<Department, Integer> type;
}
