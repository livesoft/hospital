package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-01T16:37:38.287+0800")
@StaticMetamodel(KpiPerformanceAssessment.class)
public class KpiPerformanceAssessment_ {
	public static volatile SingularAttribute<KpiPerformanceAssessment, Integer> id;
	public static volatile SingularAttribute<KpiPerformanceAssessment, Integer> indexScore;
	public static volatile SingularAttribute<KpiPerformanceAssessment, PerformanceAssessment> performanceAssessment;
	public static volatile SingularAttribute<KpiPerformanceAssessment, KpiIndex> kpiIndex;
}
