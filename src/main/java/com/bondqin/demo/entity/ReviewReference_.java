package com.bondqin.demo.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-29T23:57:51.460+0800")
@StaticMetamodel(ReviewReference.class)
public class ReviewReference_ {
	public static volatile SingularAttribute<ReviewReference, Integer> id;
	public static volatile SingularAttribute<ReviewReference, String> referenceContent;
	public static volatile SingularAttribute<ReviewReference, Date> referenceDate;
	public static volatile SingularAttribute<ReviewReference, String> referenceName;
	public static volatile SingularAttribute<ReviewReference, Integer> referenceType;
	public static volatile SingularAttribute<ReviewReference, Employee> employee;
	public static volatile SingularAttribute<ReviewReference, Integer> employeeID;
}
