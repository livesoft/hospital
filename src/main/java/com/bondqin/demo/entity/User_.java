package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-26T22:25:13.727+0800")
@StaticMetamodel(User.class)
public class User_ {
	public static volatile SingularAttribute<User, String> username;
	public static volatile SingularAttribute<User, Integer> enabled;
	public static volatile SingularAttribute<User, String> password;
	public static volatile ListAttribute<User, Authority> authorities;
}
