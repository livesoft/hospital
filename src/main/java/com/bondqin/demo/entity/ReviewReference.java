package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the review_reference database table.
 * 
 */
@Entity
@Table(name="review_reference")
@NamedQuery(name="ReviewReference.findAll", query="SELECT r FROM ReviewReference r")
public class ReviewReference implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String referenceContent;

	@Temporal(TemporalType.DATE)
	private Date referenceDate;

	private String referenceName;

	private Integer referenceType;

	//uni-directional one-to-one association to Employee
	@OneToOne
	@JoinColumn(name="employeeID")
	private Employee employee;
	
	@Column(updatable=false,insertable=false)
	private Integer employeeID;

	public ReviewReference() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReferenceContent() {
		return this.referenceContent;
	}

	public void setReferenceContent(String referenceContent) {
		this.referenceContent = referenceContent;
	}

	public Date getReferenceDate() {
		return this.referenceDate;
	}

	public void setReferenceDate(Date referenceDate) {
		this.referenceDate = referenceDate;
	}

	public String getReferenceName() {
		return this.referenceName;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	public Integer getReferenceType() {
		return this.referenceType;
	}

	public void setReferenceType(Integer referenceType) {
		this.referenceType = referenceType;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

}