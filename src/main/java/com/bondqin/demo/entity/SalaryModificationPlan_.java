package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-01T16:37:38.302+0800")
@StaticMetamodel(SalaryModificationPlan.class)
public class SalaryModificationPlan_ {
	public static volatile SingularAttribute<SalaryModificationPlan, Integer> id;
	public static volatile SingularAttribute<SalaryModificationPlan, Integer> modificationApplyDate;
	public static volatile SingularAttribute<SalaryModificationPlan, Integer> modificationPercent;
	public static volatile SingularAttribute<SalaryModificationPlan, Employee> employee;
}
