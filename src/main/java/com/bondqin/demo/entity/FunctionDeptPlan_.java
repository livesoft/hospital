package com.bondqin.demo.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-29T23:56:56.376+0800")
@StaticMetamodel(FunctionDeptPlan.class)
public class FunctionDeptPlan_ {
	public static volatile SingularAttribute<FunctionDeptPlan, Integer> id;
	public static volatile SingularAttribute<FunctionDeptPlan, String> addedContent;
	public static volatile SingularAttribute<FunctionDeptPlan, String> cultureBuildGoal;
	public static volatile SingularAttribute<FunctionDeptPlan, String> manageGoal;
	public static volatile SingularAttribute<FunctionDeptPlan, String> planName;
	public static volatile SingularAttribute<FunctionDeptPlan, Date> planStartDate;
	public static volatile SingularAttribute<FunctionDeptPlan, Date> planStopDate;
	public static volatile SingularAttribute<FunctionDeptPlan, Department> department;
	public static volatile SingularAttribute<FunctionDeptPlan, Integer> departmentID;
}
