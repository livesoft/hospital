package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the service_dept_plan database table.
 * 
 */
@Entity
@Table(name="service_dept_plan")
@NamedQuery(name="ServiceDeptPlan.findAll", query="SELECT s FROM ServiceDeptPlan s")
public class ServiceDeptPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String addedContent;

	private String cultureBuildGoal;

	private String financeGoal;

	private String hardwareBuildGoal;

	private String planName;

	@Temporal(TemporalType.DATE)
	private Date planStartDate;

	@Temporal(TemporalType.DATE)
	private Date planStopDate;

	private String projectGoal;

	private String talentBuildGoal;

	private String transactionGoal;

	//uni-directional many-to-one association to Department
	@ManyToOne
	@JoinColumn(name="departmentID")
	private Department department;
	
	@Column(updatable=false,insertable=false)
	private Integer departmentID;

	public ServiceDeptPlan() {
	}
	
	public Integer getDepartmentID() {
		return departmentID;
	}

	public void setDepartmentID(Integer departmentID) {
		this.departmentID = departmentID;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddedContent() {
		return this.addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public String getCultureBuildGoal() {
		return this.cultureBuildGoal;
	}

	public void setCultureBuildGoal(String cultureBuildGoal) {
		this.cultureBuildGoal = cultureBuildGoal;
	}

	public String getFinanceGoal() {
		return this.financeGoal;
	}

	public void setFinanceGoal(String financeGoal) {
		this.financeGoal = financeGoal;
	}

	public String getHardwareBuildGoal() {
		return this.hardwareBuildGoal;
	}

	public void setHardwareBuildGoal(String hardwareBuildGoal) {
		this.hardwareBuildGoal = hardwareBuildGoal;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Date getPlanStartDate() {
		return this.planStartDate;
	}

	public void setPlanStartDate(Date planStartDate) {
		this.planStartDate = planStartDate;
	}

	public Date getPlanStopDate() {
		return this.planStopDate;
	}

	public void setPlanStopDate(Date planStopDate) {
		this.planStopDate = planStopDate;
	}

	public String getProjectGoal() {
		return this.projectGoal;
	}

	public void setProjectGoal(String projectGoal) {
		this.projectGoal = projectGoal;
	}

	public String getTalentBuildGoal() {
		return this.talentBuildGoal;
	}

	public void setTalentBuildGoal(String talentBuildGoal) {
		this.talentBuildGoal = talentBuildGoal;
	}

	public String getTransactionGoal() {
		return this.transactionGoal;
	}

	public void setTransactionGoal(String transactionGoal) {
		this.transactionGoal = transactionGoal;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}