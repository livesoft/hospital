package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the annual_plan database table.
 * 
 */
@Entity
@Table(name="annual_plan")
@NamedQuery(name="AnnualPlan.findAll", query="SELECT a FROM AnnualPlan a")
public class AnnualPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer planID;

	private String addedContent;

	private String cultureBuildGoal;

	private String financeGoal;

	private String hardwareBuildGoal;

	private String manageGoal;

	private Integer planMeasure;

	private String planName;

	private String projectGoal;

	private String talentBuildGoal;

	private String transactionGoal;

	public AnnualPlan() {
	}

	public Integer getPlanID() {
		return this.planID;
	}

	public void setPlanID(Integer planID) {
		this.planID = planID;
	}

	public String getAddedContent() {
		return this.addedContent;
	}

	public void setAddedContent(String addedContent) {
		this.addedContent = addedContent;
	}

	public String getCultureBuildGoal() {
		return this.cultureBuildGoal;
	}

	public void setCultureBuildGoal(String cultureBuildGoal) {
		this.cultureBuildGoal = cultureBuildGoal;
	}

	public String getFinanceGoal() {
		return this.financeGoal;
	}

	public void setFinanceGoal(String financeGoal) {
		this.financeGoal = financeGoal;
	}

	public String getHardwareBuildGoal() {
		return this.hardwareBuildGoal;
	}

	public void setHardwareBuildGoal(String hardwareBuildGoal) {
		this.hardwareBuildGoal = hardwareBuildGoal;
	}

	public String getManageGoal() {
		return this.manageGoal;
	}

	public void setManageGoal(String manageGoal) {
		this.manageGoal = manageGoal;
	}

	public Integer getPlanMeasure() {
		return this.planMeasure;
	}

	public void setPlanMeasure(Integer planMeasure) {
		this.planMeasure = planMeasure;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getProjectGoal() {
		return this.projectGoal;
	}

	public void setProjectGoal(String projectGoal) {
		this.projectGoal = projectGoal;
	}

	public String getTalentBuildGoal() {
		return this.talentBuildGoal;
	}

	public void setTalentBuildGoal(String talentBuildGoal) {
		this.talentBuildGoal = talentBuildGoal;
	}

	public String getTransactionGoal() {
		return this.transactionGoal;
	}

	public void setTransactionGoal(String transactionGoal) {
		this.transactionGoal = transactionGoal;
	}

}