package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-02T09:30:22.023+0800")
@StaticMetamodel(Employee.class)
public class Employee_ {
	public static volatile SingularAttribute<Employee, Integer> id;
	public static volatile SingularAttribute<Employee, String> employeeGender;
	public static volatile SingularAttribute<Employee, String> employeeName;
	public static volatile SingularAttribute<Employee, Double> employeeSalary;
	public static volatile SingularAttribute<Employee, String> workNo;
	public static volatile SingularAttribute<Employee, Position> position;
	public static volatile SingularAttribute<Employee, Employee> directLeader;
	public static volatile SingularAttribute<Employee, Department> department;
}
