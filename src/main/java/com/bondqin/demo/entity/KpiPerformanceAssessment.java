package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the kpi_performance_assessment database table.
 * 
 */
@Entity
@Table(name="kpi_performance_assessment")
@NamedQuery(name="KpiPerformanceAssessment.findAll", query="SELECT k FROM KpiPerformanceAssessment k")
public class KpiPerformanceAssessment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Integer indexScore;

	//bi-directional many-to-one association to PerformanceAssessment
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="performance_assessment_id")
	private PerformanceAssessment performanceAssessment;

	//uni-directional one-to-one association to KpiIndex
	@OneToOne
	@JoinColumn(name="indexID")
	private KpiIndex kpiIndex;

	public KpiPerformanceAssessment() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndexScore() {
		return this.indexScore;
	}

	public void setIndexScore(Integer indexScore) {
		this.indexScore = indexScore;
	}

	public PerformanceAssessment getPerformanceAssessment() {
		return this.performanceAssessment;
	}

	public void setPerformanceAssessment(PerformanceAssessment performanceAssessment) {
		this.performanceAssessment = performanceAssessment;
	}

	public KpiIndex getKpiIndex() {
		return this.kpiIndex;
	}

	public void setKpiIndex(KpiIndex kpiIndex) {
		this.kpiIndex = kpiIndex;
	}

}