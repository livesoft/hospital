package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-05-02T13:25:44.809+0800")
@StaticMetamodel(Task.class)
public class Task_ {
	public static volatile SingularAttribute<Task, Integer> id;
	public static volatile SingularAttribute<Task, Integer> applyDate;
	public static volatile SingularAttribute<Task, Integer> directorID;
	public static volatile SingularAttribute<Task, Integer> employeeID;
	public static volatile SingularAttribute<Task, Integer> state;
}
