package com.bondqin.demo.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-29T21:20:00.621+0800")
@StaticMetamodel(ServiceDeptPlan.class)
public class ServiceDeptPlan_ {
	public static volatile SingularAttribute<ServiceDeptPlan, Integer> id;
	public static volatile SingularAttribute<ServiceDeptPlan, String> addedContent;
	public static volatile SingularAttribute<ServiceDeptPlan, String> cultureBuildGoal;
	public static volatile SingularAttribute<ServiceDeptPlan, String> financeGoal;
	public static volatile SingularAttribute<ServiceDeptPlan, String> hardwareBuildGoal;
	public static volatile SingularAttribute<ServiceDeptPlan, String> planName;
	public static volatile SingularAttribute<ServiceDeptPlan, Date> planStartDate;
	public static volatile SingularAttribute<ServiceDeptPlan, Date> planStopDate;
	public static volatile SingularAttribute<ServiceDeptPlan, String> projectGoal;
	public static volatile SingularAttribute<ServiceDeptPlan, String> talentBuildGoal;
	public static volatile SingularAttribute<ServiceDeptPlan, String> transactionGoal;
	public static volatile SingularAttribute<ServiceDeptPlan, Department> department;
	public static volatile SingularAttribute<ServiceDeptPlan, Integer> departmentID;
}
