package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the kpi_index database table.
 * 
 */
@Entity
@Table(name="kpi_index")
@NamedQuery(name="KpiIndex.findAll", query="SELECT k FROM KpiIndex k")
public class KpiIndex implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Integer indexCategoryID;

	private String indexCategoryName;

	private Integer indexComparativeWeight;

	private String indexName;

	private String indexRemark;

	public KpiIndex() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndexCategoryID() {
		return this.indexCategoryID;
	}

	public void setIndexCategoryID(Integer indexCategoryID) {
		this.indexCategoryID = indexCategoryID;
	}

	public String getIndexCategoryName() {
		return this.indexCategoryName;
	}

	public void setIndexCategoryName(String indexCategoryName) {
		this.indexCategoryName = indexCategoryName;
	}

	public Integer getIndexComparativeWeight() {
		return this.indexComparativeWeight;
	}

	public void setIndexComparativeWeight(Integer indexComparativeWeight) {
		this.indexComparativeWeight = indexComparativeWeight;
	}

	public String getIndexName() {
		return this.indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getIndexRemark() {
		return this.indexRemark;
	}

	public void setIndexRemark(String indexRemark) {
		this.indexRemark = indexRemark;
	}

}