package com.bondqin.demo.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-04-30T23:01:24.844+0800")
@StaticMetamodel(KpiIndex.class)
public class KpiIndex_ {
	public static volatile SingularAttribute<KpiIndex, Integer> id;
	public static volatile SingularAttribute<KpiIndex, Integer> indexCategoryID;
	public static volatile SingularAttribute<KpiIndex, String> indexCategoryName;
	public static volatile SingularAttribute<KpiIndex, Integer> indexComparativeWeight;
	public static volatile SingularAttribute<KpiIndex, String> indexName;
	public static volatile SingularAttribute<KpiIndex, String> indexRemark;
}
