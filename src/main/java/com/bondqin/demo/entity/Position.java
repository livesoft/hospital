package com.bondqin.demo.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the position database table.
 * 
 */
@Entity
@NamedQuery(name="Position.findAll", query="SELECT p FROM Position p")
public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String positionContent;

	private Integer positionDepartment;

	private String positionFunction;

	private String positionName;

	private Integer positionSalary;

	public Position() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPositionContent() {
		return this.positionContent;
	}

	public void setPositionContent(String positionContent) {
		this.positionContent = positionContent;
	}

	public Integer getPositionDepartment() {
		return this.positionDepartment;
	}

	public void setPositionDepartment(Integer positionDepartment) {
		this.positionDepartment = positionDepartment;
	}

	public String getPositionFunction() {
		return this.positionFunction;
	}

	public void setPositionFunction(String positionFunction) {
		this.positionFunction = positionFunction;
	}

	public String getPositionName() {
		return this.positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public Integer getPositionSalary() {
		return this.positionSalary;
	}

	public void setPositionSalary(Integer positionSalary) {
		this.positionSalary = positionSalary;
	}

}