package com.bondqin.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bondqin.demo.entity.AnnualPlan;

/**
 * 医院年度计划Repository
 * 
 * 参看文档
 * http://docs.spring.io/spring-data/commons/docs/current/reference/html/#repositories.namespace-reference
 * @author bond
 *
 */
@Repository
public interface AnnualPlanDao extends JpaRepository<AnnualPlan, Integer>, JpaSpecificationExecutor<AnnualPlan> {

}
