package com.bondqin.demo.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bondqin.demo.entity.User;

/**
 * 医院年度计划Repository
 * 
 * 参看文档
 * http://docs.spring.io/spring-data/commons/docs/current/reference/html/#repositories.namespace-reference
 * @author bond
 *
 */
@Repository
public interface UserDao extends CrudRepository<User, String> {

}
