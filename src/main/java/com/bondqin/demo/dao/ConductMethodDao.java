package com.bondqin.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bondqin.demo.entity.ConductMethod;

/**
 * 
 * �ο��ĵ�
 * http://docs.spring.io/spring-data/commons/docs/current/reference/html/#repositories.namespace-reference
 * @author bond
 *
 */
@Repository
public interface ConductMethodDao extends JpaRepository<ConductMethod, Integer>, JpaSpecificationExecutor<ConductMethod> {

	public List<ConductMethod> findByConductApplyDate(Integer conductApplyDate);
}
