package com.bondqin.demo.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bondqin.demo.entity.AnnualPlan;
import com.bondqin.demo.entity.FunctionDeptPlan;

/**
 * 
 * �ο��ĵ�
 * http://docs.spring.io/spring-data/commons/docs/current/reference/html/#repositories.namespace-reference
 * @author bond
 *
 */
@Repository
public interface FunctionDeptPlanDao extends JpaRepository<FunctionDeptPlan, Integer>, JpaSpecificationExecutor<AnnualPlan> {

	public Page<FunctionDeptPlan> findByDepartmentID(Integer departmentID, Pageable pageable);
}
