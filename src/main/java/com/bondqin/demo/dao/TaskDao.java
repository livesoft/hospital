package com.bondqin.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.bondqin.demo.entity.Task;

/**
 * 
 * �ο��ĵ�
 * http://docs.spring.io/spring-data/commons/docs/current/reference/html/#repositories.namespace-reference
 * @author bond
 *
 */
@Repository
public interface TaskDao extends JpaRepository<Task, Integer>, JpaSpecificationExecutor<Task> {
	
	
}
