package com.bondqin.demo.config;

import org.apache.commons.lang.ObjectUtils;

/**
 *
 */
public enum TaskStateEnum {

	NEW_STATE(0, "未处理"),
	
	FINISH_STATE(1, "已处理");
	
	private Object value;
	
	private String text;
	
	TaskStateEnum(Object value, String text) {
		this.value = value;
		this.text = text;
	}
	
	public static String getText(Object value) {
		for (TaskStateEnum role : TaskStateEnum.values()) {
			if (ObjectUtils.equals(value, role.value)) {
				return role.text;
			}
		}
		return null;
	}
}
