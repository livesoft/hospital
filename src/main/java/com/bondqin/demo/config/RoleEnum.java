package com.bondqin.demo.config;

import org.apache.commons.lang.ObjectUtils;

/**
 * 角色
 *
 */
public enum RoleEnum {

	ROLE_R1("ROLE_R1", "院办"),
	
	ROLE_R2("ROLE_R2", "人事部门"),
	
	ROLE_R3("ROLE_R3", "科长"),
	
	ROLE_R4("ROLE_R4", "直接领导"),
	
	ROLE_R5("ROLE_R5", "员工");
	
	private Object value;
	
	private String text;
	
	RoleEnum(Object value, String text) {
		this.value = value;
		this.text = text;
	}
	
	public static String getText(Object value) {
		for (RoleEnum role : RoleEnum.values()) {
			if (ObjectUtils.equals(value, role.value)) {
				return role.text;
			}
		}
		return null;
	}
}
