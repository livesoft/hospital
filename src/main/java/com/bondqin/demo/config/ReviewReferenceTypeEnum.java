package com.bondqin.demo.config;

import org.apache.commons.lang.ObjectUtils;

/**
 *
 */
public enum ReviewReferenceTypeEnum {

	CHENGJI(1, "�ɼ�"),
	
	BUZU(2, "����"),
	
	PEIXUN(2, "��ѵ");
	
	private Object value;
	
	private String text;
	
	ReviewReferenceTypeEnum(Object value, String text) {
		this.value = value;
		this.text = text;
	}
	
	public static String getText(Object value) {
		for (ReviewReferenceTypeEnum role : ReviewReferenceTypeEnum.values()) {
			if (ObjectUtils.equals(value, role.value)) {
				return role.text;
			}
		}
		return null;
	}
}
