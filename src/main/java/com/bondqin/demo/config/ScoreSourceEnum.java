package com.bondqin.demo.config;

import org.apache.commons.lang.ObjectUtils;

/**
 * 角色
 *
 */
public enum ScoreSourceEnum {

	DIRECT_SOURCE(1, "领导评分"),
	
	EMPLOYEE_SOURCE(1, "同事评分");
	
	private Object value;
	
	private String text;
	
	ScoreSourceEnum(Object value, String text) {
		this.value = value;
		this.text = text;
	}
	
	public static String getText(Object value) {
		for (ScoreSourceEnum role : ScoreSourceEnum.values()) {
			if (ObjectUtils.equals(value, role.value)) {
				return role.text;
			}
		}
		return null;
	}
}
