package com.bondqin.demo.config;

import org.apache.commons.lang.ObjectUtils;

/**
 * 角色
 *
 */
public enum DepartmentTypeEnum {

	SERVICE(1, "业务科室"),
	
	FUNCTION(2, "职能科室"),
	
	BOSS(9, "院办");
	
	private Object value;
	
	private String text;
	
	DepartmentTypeEnum(Object value, String text) {
		this.value = value;
		this.text = text;
	}
	
	public static String getText(Object value) {
		for (DepartmentTypeEnum role : DepartmentTypeEnum.values()) {
			if (ObjectUtils.equals(value, role.value)) {
				return role.text;
			}
		}
		return null;
	}
}
