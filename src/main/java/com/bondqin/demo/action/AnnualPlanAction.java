package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.AnnualPlanDao;
import com.bondqin.demo.entity.AnnualPlan;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.AnnualPlanVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 医院年度计划
 * @author bond
 *
 */
@Service
public class AnnualPlanAction {

	@Autowired
	private AnnualPlanDao annualPlanDao;

	/**
	 * 分页列出所有医院年度计划
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String, Object> listAll(Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<AnnualPlan> page = annualPlanDao.findAll(pageable);

		Page<AnnualPlanVo> pageVo = ModelUtil.toPage(page, AnnualPlanVo.class);
		
		return ModelUtil.toDatatable(pageVo);
	}
	
	/**
	 * 列出某一个医院年度计划
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		AnnualPlan source = annualPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		AnnualPlanVo vo = new AnnualPlanVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(AnnualPlanVo record) {
		ResultModel result = new ResultModel();
		
		AnnualPlan entity = new AnnualPlan();
		BeanUtils.copyProperties(record, entity);

		annualPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 修改年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(AnnualPlanVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getPlanID();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		AnnualPlan source = annualPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		AnnualPlan entity = new AnnualPlan();
		BeanUtils.copyProperties(record, entity);

		annualPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 删除年度计划
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		AnnualPlan source = annualPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		annualPlanDao.delete(id);
		
		return result;
        
	}
}