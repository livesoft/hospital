package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.FunctionDeptPlanDao;
import com.bondqin.demo.entity.Department;
import com.bondqin.demo.entity.FunctionDeptPlan;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.FunctionDeptPlanVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * @author bond
 *
 */
@Service
public class FunctionDeptPlanAction {

	@Autowired
	private FunctionDeptPlanDao functionDeptPlanDao;

	/**
	 * 分页列出
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String, Object> listAll(Integer departmentID, Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<FunctionDeptPlan> page = null;
		if (departmentID == null) {
			page = functionDeptPlanDao.findAll(pageable);
		} else {
			page = functionDeptPlanDao.findByDepartmentID(departmentID, pageable);
		}
				
		Page<FunctionDeptPlanVo> pageVo = ModelUtil.toPage(page, FunctionDeptPlanVo.class);
		
		return ModelUtil.toDatatable(pageVo);
	}
	
	/**
	 * 列出某一个
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		FunctionDeptPlan source = functionDeptPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		FunctionDeptPlanVo vo = new FunctionDeptPlanVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(Integer departmentID, FunctionDeptPlanVo record) {
		ResultModel result = new ResultModel();
		
		FunctionDeptPlan entity = new FunctionDeptPlan();
		BeanUtils.copyProperties(record, entity);
		
		Department dept = new Department();
		dept.setId(departmentID);
		entity.setDepartment(dept);

		functionDeptPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(FunctionDeptPlanVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getId();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		FunctionDeptPlan source = functionDeptPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		FunctionDeptPlan entity = new FunctionDeptPlan();
		BeanUtils.copyProperties(record, entity);
		
		entity.setDepartment(source.getDepartment());

		functionDeptPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		FunctionDeptPlan source = functionDeptPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		functionDeptPlanDao.delete(id);
		
		return result;
        
	}
}