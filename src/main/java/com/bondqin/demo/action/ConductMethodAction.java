package com.bondqin.demo.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.ConductAdoptKpiDao;
import com.bondqin.demo.dao.ConductMethodDao;
import com.bondqin.demo.entity.ConductAdoptKpi;
import com.bondqin.demo.entity.ConductMethod;
import com.bondqin.demo.entity.KpiIndex;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.ConductAdoptKpiVo;
import com.bondqin.demo.vo.ConductMethodVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * @author bond
 *
 */
@Service
public class ConductMethodAction {

	@Autowired
	private ConductMethodDao conductMethodDao;
	
	@Autowired
	private ConductAdoptKpiDao conductAdoptKpi;

	/**
	 * 分页列出所有医院年度计划
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String, Object> listAll(Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<ConductMethod> page = conductMethodDao.findAll(pageable);

		
		Page<ConductMethodVo> pageVo = page.map(
			new Converter<ConductMethod, ConductMethodVo>() {

				@Override
				public ConductMethodVo convert(ConductMethod source) {
					ConductMethodVo vo = new ConductMethodVo();
					vo.setId(source.getId());
					vo.setConductName(source.getConductName());
					vo.setConductApplyDate(source.getConductApplyDate());
					return vo;
				}
		});
		
		return ModelUtil.toDatatable(pageVo);
	}
	
	/**
	 * 列出某一个医院年度计划
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ConductMethod source = conductMethodDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		ConductMethodVo vo = new ConductMethodVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(ConductMethodVo record) {
		ResultModel result = new ResultModel();
		
		ConductMethod entity = new ConductMethod();
		BeanUtils.copyProperties(record, entity);

		setKpi(record, entity);

		conductMethodDao.save(entity);
		
		return result;
	}
	
	private void setKpi(ConductMethodVo record, ConductMethod entity) {
		List<ConductAdoptKpiVo> serviceKpiList = record.getServiceKpis();
		List<ConductAdoptKpiVo> functionKpiList = record.getFunctionKpis();
		
		List<ConductAdoptKpi> list = new ArrayList<ConductAdoptKpi>();
		
		for (ConductAdoptKpiVo vo : serviceKpiList) {
			if (vo.getKpiIndex() != null) {
				ConductAdoptKpi kpi = new ConductAdoptKpi();
				kpi.setIndexCategoryWeight(vo.getIndexCategoryWeight());
				kpi.setType(vo.getType());
				KpiIndex index = new KpiIndex();
				index.setId(vo.getKpiIndex());
				kpi.setKpiIndex(index);
				kpi.setConductMethod(entity);
				
				list.add(kpi);
			}
		}
		for (ConductAdoptKpiVo vo : functionKpiList) {
			if (vo.getKpiIndex() != null) {
				ConductAdoptKpi kpi = new ConductAdoptKpi();
				kpi.setIndexCategoryWeight(vo.getIndexCategoryWeight());
				kpi.setType(vo.getType());
				KpiIndex index = new KpiIndex();
				index.setId(vo.getKpiIndex());
				kpi.setKpiIndex(index);
				kpi.setConductMethod(entity);
				
				list.add(kpi);
			}
		}
		
		entity.setConductAdoptKpis(list);
		
	}
	
	/**
	 * 修改年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(ConductMethodVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getId();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ConductMethod source = conductMethodDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		ConductMethod entity = new ConductMethod();
		BeanUtils.copyProperties(record, entity);
		
		conductAdoptKpi.delete(source.getConductAdoptKpis());
		
		setKpi(record, entity);
		
		conductMethodDao.save(entity);
		return result;
	}
	
	/**
	 * 删除年度计划
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ConductMethod source = conductMethodDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		conductMethodDao.delete(id);
		
		return result;
        
	}
}