package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.ServiceDeptPlanDao;
import com.bondqin.demo.entity.Department;
import com.bondqin.demo.entity.ServiceDeptPlan;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.ResultModel;
import com.bondqin.demo.vo.ServiceDeptPlanVo;

/**
 * @author bond
 *
 */
@Service
public class ServiceDeptPlanAction {

	@Autowired
	private ServiceDeptPlanDao serviceDeptPlanDao;

	/**
	 * 分页列出
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String,Object> listAll(Integer departmentID, Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<ServiceDeptPlan> page = null;
		if (departmentID == null) {
			page = serviceDeptPlanDao.findAll(pageable);
		} else {
			page = serviceDeptPlanDao.findByDepartmentID(departmentID, pageable);
		}
				
		Page<ServiceDeptPlanVo> pageVo = ModelUtil.toPage(page, ServiceDeptPlanVo.class);
		
		return ModelUtil.toDatatable(pageVo);
	}
	
	/**
	 * 列出某一个
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ServiceDeptPlan source = serviceDeptPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		ServiceDeptPlanVo vo = new ServiceDeptPlanVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(Integer departmentID, ServiceDeptPlanVo record) {
		ResultModel result = new ResultModel();
		
		ServiceDeptPlan entity = new ServiceDeptPlan();
		BeanUtils.copyProperties(record, entity);
		
		Department dept = new Department();
		dept.setId(departmentID);
		entity.setDepartment(dept);

		serviceDeptPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(ServiceDeptPlanVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getId();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ServiceDeptPlan source = serviceDeptPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		ServiceDeptPlan entity = new ServiceDeptPlan();
		BeanUtils.copyProperties(record, entity);
		
		entity.setDepartment(source.getDepartment());

		serviceDeptPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ServiceDeptPlan source = serviceDeptPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		serviceDeptPlanDao.delete(id);
		
		return result;
        
	}
}