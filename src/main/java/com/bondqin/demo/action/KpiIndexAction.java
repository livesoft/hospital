package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.KpiIndexDao;
import com.bondqin.demo.entity.KpiIndex;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.KpiIndexVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 医院年度计划
 * @author bond
 *
 */
@Service
public class KpiIndexAction {

	@Autowired
	private KpiIndexDao kpiIndexDao;

	/**
	 * 分页列出所有医院年度计划
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String, Object> listAll(Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<KpiIndex> page = kpiIndexDao.findAll(pageable);

		Page<KpiIndexVo> pageVo = ModelUtil.toPage(page, KpiIndexVo.class);
		
		return ModelUtil.toDatatable(pageVo);
	}
	
	/**
	 * 列出某一个医院年度计划
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		KpiIndex source = kpiIndexDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		KpiIndexVo vo = new KpiIndexVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(KpiIndexVo record) {
		ResultModel result = new ResultModel();
		
		KpiIndex entity = new KpiIndex();
		BeanUtils.copyProperties(record, entity);

		kpiIndexDao.save(entity);
		
		return result;
	}
	
	/**
	 * 修改年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(KpiIndexVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getId();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		KpiIndex source = kpiIndexDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		KpiIndex entity = new KpiIndex();
		BeanUtils.copyProperties(record, entity);

		kpiIndexDao.save(entity);
		
		return result;
	}
	
	/**
	 * 删除年度计划
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		KpiIndex source = kpiIndexDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		kpiIndexDao.delete(id);
		
		return result;
        
	}
}