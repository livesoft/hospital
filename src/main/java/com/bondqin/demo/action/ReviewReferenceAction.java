package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.ReviewReferenceDao;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.entity.ReviewReference;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.ResultModel;
import com.bondqin.demo.vo.ReviewReferenceVo;

/**
 * @author bond
 *
 */
@Service
public class ReviewReferenceAction {

	@Autowired
	private ReviewReferenceDao reviewReferenceDao;

	/**
	 * 分页列出
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String, Object> listAll(Integer employee, Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<ReviewReference> page = reviewReferenceDao.findByEmployeeID(employee, pageable);
				
		Page<ReviewReferenceVo> pageVo = ModelUtil.toPage(page, ReviewReferenceVo.class);
		
		return ModelUtil.toDatatable(pageVo);
	}
	
	/**
	 * 列出某一个
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ReviewReference source = reviewReferenceDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		ReviewReferenceVo vo = new ReviewReferenceVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(Integer employeeID, ReviewReferenceVo record) {
		ResultModel result = new ResultModel();
		
		ReviewReference entity = new ReviewReference();
		BeanUtils.copyProperties(record, entity);
		
		Employee employee = new Employee();
		employee.setId(employeeID);
		entity.setEmployee(employee);

		reviewReferenceDao.save(entity);
		
		return result;
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(ReviewReferenceVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getId();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ReviewReference source = reviewReferenceDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		ReviewReference entity = new ReviewReference();
		BeanUtils.copyProperties(record, entity);
		
		entity.setEmployee(source.getEmployee());

		reviewReferenceDao.save(entity);
		
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		ReviewReference source = reviewReferenceDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		reviewReferenceDao.delete(id);
		
		return result;
        
	}
}