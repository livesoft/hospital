package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.PersonalPlanDao;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.entity.PersonalPlan;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.PersonalPlanVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * @author bond
 *
 */
@Service
public class PersonalPlanAction {

	@Autowired
	private PersonalPlanDao personalPlanDao;

	/**
	 * 分页列出
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String, Object> listAll(Integer employee, Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<PersonalPlan> page = personalPlanDao.findByEmployeeID(employee, pageable);
				
		Page<PersonalPlanVo> pageVo = ModelUtil.toPage(page, PersonalPlanVo.class);
		
		return ModelUtil.toDatatable(pageVo);
	}
	
	/**
	 * 列出某一个
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		PersonalPlan source = personalPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		PersonalPlanVo vo = new PersonalPlanVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(Integer employeeID, PersonalPlanVo record) {
		ResultModel result = new ResultModel();
		
		PersonalPlan entity = new PersonalPlan();
		BeanUtils.copyProperties(record, entity);
		
		Employee employee = new Employee();
		employee.setId(employeeID);
		entity.setEmployee(employee);

		personalPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 修改
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(PersonalPlanVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getId();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		PersonalPlan source = personalPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		PersonalPlan entity = new PersonalPlan();
		BeanUtils.copyProperties(record, entity);
		
		entity.setEmployee(source.getEmployee());

		personalPlanDao.save(entity);
		
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		PersonalPlan source = personalPlanDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		personalPlanDao.delete(id);
		
		return result;
        
	}
}