package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bondqin.demo.dao.TaskDao;
import com.bondqin.demo.entity.Task;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.TaskVo;

/**
 * 医院年度计划
 * @author bond
 *
 */
@Service
public class TaskAction {

	@Autowired
	private TaskDao taskDao;

	public Map<String, Object> listAll(Integer number, Integer length) {
		Pageable pageable = new PageRequest(number, length);
		Page<Task> page = taskDao.findAll(pageable);
				
		Page<TaskVo> pageVo = ModelUtil.toPage(page, TaskVo.class);
		
		return ModelUtil.toDatatable(pageVo);
	}
	
}