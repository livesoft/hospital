package com.bondqin.demo.action;

import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.WorkReportDao;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.entity.WorkReport;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.ResultModel;
import com.bondqin.demo.vo.WorkReportVo;

/**
 * 医院年度计划
 * @author bond
 *
 */
@Service
public class WorkReportAction {

	@Autowired
	private WorkReportDao workReportDao;

	/**
	 * 分页列出所有医院年度计划
	 * @param number	当前页码
	 * @param size		每页个数
	 * @return	
	 */
	public Map<String, Object> listAll(Integer employeeID,Integer number, Integer length) {
		try{
		Pageable pageable = new PageRequest(number, length);
		Page<WorkReport> page = workReportDao.findByEmployeeID(employeeID, pageable);

		Page<WorkReportVo> pageVo = ModelUtil.toPage(page, WorkReportVo.class);
		
		return ModelUtil.toDatatable(pageVo);
		}catch(Exception ex){
			return null;
		}
	}
	
	/**
	 * 列出某一个医院年度计划
	 * @param id	年度计划ID
	 * @return
	 */
	public ResultModel listOne(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		WorkReport source = workReportDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		WorkReportVo vo = new WorkReportVo();
		BeanUtils.copyProperties(source, vo);
		
		result.put("data", vo);
		return result;
	}

	/**
	 * 新增年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(Employee employee, WorkReportVo record) {
		ResultModel result = new ResultModel();
		
		WorkReport entity = new WorkReport();
		BeanUtils.copyProperties(record, entity);
		entity.setEmployee(employee);
		
		workReportDao.save(entity);
		
		return result;
	}
	
	/**
	 * 修改年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel modify(WorkReportVo record) {
		ResultModel result = new ResultModel();
		
		Integer id = record.getId();
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		WorkReport source = workReportDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		WorkReport entity = new WorkReport();
		BeanUtils.copyProperties(record, entity);
		entity.setEmployee(source.getEmployee());
		workReportDao.save(entity);
		
		return result;
	}
	
	/**
	 * 删除年度计划
	 * @param id
	 * @return
	 */
	@Transactional
	public ResultModel remove(Integer id) {
		ResultModel result = new ResultModel();
		
		if (id == null) {
			result.addError(MessagesUtil.getMessage("Message.null", "ID"));
			return result;
		}
		WorkReport source = workReportDao.findOne(id);
		if (source == null) {
			result.addError(MessagesUtil.getMessage("Message.notExists", "ID"));
			return result;
		}
		
		workReportDao.delete(id);
		
		return result;
        
	}
}