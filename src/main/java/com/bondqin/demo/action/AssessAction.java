package com.bondqin.demo.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bondqin.demo.dao.PerformanceAssessmentDao;
import com.bondqin.demo.entity.KpiIndex;
import com.bondqin.demo.entity.KpiPerformanceAssessment;
import com.bondqin.demo.entity.PerformanceAssessment;
import com.bondqin.demo.vo.KpiPerformanceAssessmentVo;
import com.bondqin.demo.vo.PerformanceAssessmentVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 医院年度计划
 * @author bond
 *
 */
@Service
public class AssessAction {

	@Autowired
	private PerformanceAssessmentDao performanceAssessmentDao;

	
	/**
	 * 新增年度计划
	 * @param record
	 * @return
	 */
	@Transactional
	public ResultModel add(PerformanceAssessmentVo record) {
		ResultModel result = new ResultModel();
		
		PerformanceAssessment entity = new PerformanceAssessment();
		BeanUtils.copyProperties(record, entity);
		
		List<KpiPerformanceAssessmentVo> voList = record.getKpiAssessList();
		
		entity.setKpiPerformanceAssessments(new ArrayList<KpiPerformanceAssessment>());
		if (voList != null) {
			for (KpiPerformanceAssessmentVo vo : voList) {
				KpiPerformanceAssessment kpiEntity = new KpiPerformanceAssessment();
				kpiEntity.setIndexScore(vo.getIndexScore());
				
				KpiIndex kpiIndex = new KpiIndex();
				kpiIndex.setId(vo.getKpiIndex());
				kpiEntity.setKpiIndex(kpiIndex);
				
				entity.addKpiPerformanceAssessment(kpiEntity);
			}
		}

		performanceAssessmentDao.save(entity);
		
		return result;
	}
	
}