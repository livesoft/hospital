package com.bondqin.demo.controller.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bondqin.demo.action.ConductMethodAction;
import com.bondqin.demo.controller.BaseController;
import com.bondqin.demo.dao.EmployeeDao;
import com.bondqin.demo.entity.Department;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.entity.KpiIndex;
import com.bondqin.demo.service.EmployeeService;
import com.bondqin.demo.service.MethodConductService;
import com.bondqin.demo.vo.ConductMethodVo;
import com.bondqin.demo.vo.KpiIndexScoreVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 全部返回json格式数据
 * @author bond
 *
 */
@RestController
@RequestMapping(value="/conductMethod")
public class ConductMethodRest extends BaseController {
	
	@Autowired
	private ConductMethodAction conductMethodAction;
	
	@Autowired
	private MethodConductService methodConductService;
	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private EmployeeService employeeService;

	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listAll.json")
	public Map<String, Object> listAll(@RequestParam(value="start", defaultValue="0") Integer start,
			@RequestParam(value="length", defaultValue="10") Integer length) {
		return conductMethodAction.listAll(start/length, length);
	}
	
	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listOne.json")
	public Map<String, Object> listOne(@RequestParam(value="id") Integer id) {
		ResultModel result = conductMethodAction.listOne(id);
		
		return result.toMap();
	}	
	
	/**
	 * 根据季度查询
	 * @param date
	 * @return
	 */
	@RequestMapping(value="listByDate.json")
	public Map<String, Object> listByDate(@RequestParam(value="employeeID") Integer employeeID,
			@RequestParam(value="applyDate") Integer applyDate) {
		Employee employee = employeeDao.findOne(employeeID);
		List<KpiIndex> list = 
				methodConductService.queryByApplyDate(applyDate, getDeptType(employee));
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("recordsTotal", list.size());
		result.put("recordsFiltered", list.size());
		result.put("data", list);

		return result;
	}
	
	@RequestMapping(value="listByDateMyself.json")
	public Map<String, Object> listByDateMyself(@RequestParam(value="applyDate") Integer applyDate) {
		List<KpiIndex> list = 
				methodConductService.queryByApplyDate(applyDate, getDeptType(getEmployee()));
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("recordsTotal", list.size());
		result.put("recordsFiltered", list.size());
		result.put("data", list);

		return result;
	}
	
	/**
	 * 根据季度查询
	 * @param date
	 * @return
	 */
	@RequestMapping(value="listByDateMyselfScore.json")
	public Map<String, Object> listByDateMyselfScore(@RequestParam(value="applyDate") Integer applyDate) {
		Employee employee = getEmployee();
		
		List<KpiIndex> kpiList = 
				methodConductService.queryByApplyDate(applyDate, getDeptType(employee));
		
		List<KpiIndexScoreVo> voList = employeeService.queryIndexScore(applyDate, kpiList);
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("totalScore", employeeService.queryTotalScore(applyDate, employee));
		result.put("recordsTotal", voList.size());
		result.put("recordsFiltered", voList.size());
		result.put("data", voList);

		return result;
	}
	
	private Integer getDeptType(Employee employee) {
		if (employee != null) {
			Department dept = employee.getDepartment();
			if (dept != null) {
				return dept.getType();
			}
		}
		return null;
	}

	/**
	 * 新增
	 * @param person
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value="add.json")
	public Map<String, Object> add(ConductMethodVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		ResultModel result = conductMethodAction.add(record);
		
		return result.toMap();
	}
	
	/**
	 * 删除
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="remove.json")
	public Map<String, Object> remove(@RequestParam(value="id") Integer id) {
		ResultModel result = conductMethodAction.remove(id);
		
		return result.toMap();
	}
	
	/**
	 * 修改
	 * @param record
	 * @param bindingResult
	 * @param model
	 * @return
	 */
	@RequestMapping(value="modify.json")
	public Map<String, Object> modify(ConductMethodVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		ResultModel result = conductMethodAction.modify(record);
		
		return result.toMap();
	}
}
