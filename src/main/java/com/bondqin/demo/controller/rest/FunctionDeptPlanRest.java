package com.bondqin.demo.controller.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bondqin.demo.action.FunctionDeptPlanAction;
import com.bondqin.demo.config.RoleEnum;
import com.bondqin.demo.controller.BaseController;
import com.bondqin.demo.entity.Department;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.util.ModelUtil;
import com.bondqin.demo.vo.FunctionDeptPlanVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 医院年度计划Controller
 * 全部返回json格式数据
 * @author bond
 *
 */
@RestController
@RequestMapping(value="/functionDeptPlan")
public class FunctionDeptPlanRest extends BaseController {
	
	@Autowired
	private FunctionDeptPlanAction functionDeptPlanAction;

	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listAll.json")
	public Map<String, Object> listAll(@RequestParam(value="start", defaultValue="0") Integer start,
			@RequestParam(value="length", defaultValue="10") Integer length) {
		Integer deptId = null;
		
		//科长
		if (request.isUserInRole(RoleEnum.ROLE_R3.name())) {
			Employee employee = getEmployee();
			
			Department dept = employee.getDepartment();
			if (dept == null) {
				return ModelUtil.emptyDatatable();
			}
			deptId = dept.getId();
		}
		return functionDeptPlanAction.listAll(deptId, start/length, length);
		
	}
	
	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listOne.json")
	public Map<String, Object> listOne(@RequestParam(value="id") Integer id) {
		ResultModel result = functionDeptPlanAction.listOne(id);
		
		return result.toMap();
	}	

	/**
	 * 新增
	 * @param person
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value="add.json")
	public Map<String, Object> add(FunctionDeptPlanVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		Employee employee = getEmployee();
		
		Department dept = employee.getDepartment();
		if (dept == null) {
			ResultModel result = new ResultModel();
			result.addError("部门为空");
			return result.toMap();
		}
		
		ResultModel result = functionDeptPlanAction.add(dept.getId(), record);
		
		return result.toMap();
	}
	
	/**
	 * 删除
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="remove.json")
	public Map<String, Object> remove(@RequestParam(value="id") Integer id) {
		ResultModel result = functionDeptPlanAction.remove(id);
		
		return result.toMap();
	}
	
	/**
	 * 修改
	 * @param record
	 * @param bindingResult
	 * @param model
	 * @return
	 */
	@RequestMapping(value="modify.json")
	public Map<String, Object> modify(FunctionDeptPlanVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		ResultModel result = functionDeptPlanAction.modify(record);
		
		return result.toMap();
	}
}
