package com.bondqin.demo.controller.rest;

import java.util.Map;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

import com.bondqin.demo.vo.ResultModel;

/**
 * 拦截RestController，处理json请求
 * 该类支持jsonp
 * 处理异常返回json格式数据
 * @author bond
 *
 */
@ControllerAdvice(annotations={RestController.class})
public class JsonControllerAdvice extends AbstractJsonpResponseBodyAdvice {  
  
	public JsonControllerAdvice() {  
        super("callback", "jsonp"); //指定jsonpParameterNames  
    }   
    
    /** 
     * Json异常页面控制 
     *  
     * @param runtimeException 
     * @return 
     */  
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String,Object> exceptionHandler(Exception exception) {  
    	ResultModel result = new ResultModel();
    	result.addError(exception.getMessage());
    	
        return result.toMap();  
    } 
}  