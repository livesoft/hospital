package com.bondqin.demo.controller.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bondqin.demo.action.AssessAction;
import com.bondqin.demo.controller.BaseController;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.vo.PerformanceAssessmentVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 全部返回json格式数据
 * @author bond
 *
 */
@RestController
@RequestMapping(value="/assess")
public class AssessRest extends BaseController {
	
	@Autowired
	private AssessAction assessAction;

	/**
	 * 新增
	 * @param person
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value="add.json")
	public Map<String, Object> add(PerformanceAssessmentVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		Employee employee = getEmployee();
		record.setScoreEmployeeID(employee.getId());
		ResultModel result = assessAction.add(record);
		
		return result.toMap();
	}
	
}
