package com.bondqin.demo.controller.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bondqin.demo.action.AnnualPlanAction;
import com.bondqin.demo.controller.BaseController;
import com.bondqin.demo.vo.AnnualPlanVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 医院年度计划Controller
 * 全部返回json格式数据
 * @author bond
 *
 */
@RestController
@RequestMapping(value="/annualPlan")
public class AnnualPlanRest extends BaseController {
	
	@Autowired
	private AnnualPlanAction annualPlanAction;

	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listAll.json")
	public Map<String, Object> listAll(@RequestParam(value="start", defaultValue="0") Integer start,
			@RequestParam(value="length", defaultValue="10") Integer length) {
		return annualPlanAction.listAll(start/length, length);
	}
	
	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listOne.json")
	public Map<String, Object> listOne(@RequestParam(value="id") Integer id) {
		ResultModel result = annualPlanAction.listOne(id);
		
		return result.toMap();
	}	

	/**
	 * 新增
	 * @param person
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value="add.json")
	public Map<String, Object> add(AnnualPlanVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		ResultModel result = annualPlanAction.add(record);
		
		return result.toMap();
	}
	
	/**
	 * 删除
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="remove.json")
	public Map<String, Object> remove(@RequestParam(value="id") Integer id) {
		ResultModel result = annualPlanAction.remove(id);
		
		return result.toMap();
	}
	
	/**
	 * 修改
	 * @param record
	 * @param bindingResult
	 * @param model
	 * @return
	 */
	@RequestMapping(value="modify.json")
	public Map<String, Object> modify(AnnualPlanVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		ResultModel result = annualPlanAction.modify(record);
		
		return result.toMap();
	}
}
