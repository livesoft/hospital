package com.bondqin.demo.controller.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bondqin.demo.action.KpiIndexAction;
import com.bondqin.demo.controller.BaseController;
import com.bondqin.demo.vo.KpiIndexVo;
import com.bondqin.demo.vo.ResultModel;

/**
 * 全部返回json格式数据
 * @author bond
 *
 */
@RestController
@RequestMapping(value="/kpiIndex")
public class KpiIndexRest extends BaseController {
	
	@Autowired
	private KpiIndexAction kpiIndexAction;

	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listAll.json")
	public Map<String, Object> listAll(@RequestParam(value="start", defaultValue="0") Integer start,
			@RequestParam(value="length", defaultValue="10") Integer length) {
		return kpiIndexAction.listAll(start/length, length);
	}
	
	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="listOne.json")
	public Map<String, Object> listOne(@RequestParam(value="id") Integer id) {
		ResultModel result = kpiIndexAction.listOne(id);
		
		return result.toMap();
	}	

	/**
	 * 新增
	 * @param person
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value="add.json")
	public Map<String, Object> add(KpiIndexVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		ResultModel result = kpiIndexAction.add(record);
		
		return result.toMap();
	}
	
	/**
	 * 删除
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="remove.json")
	public Map<String, Object> remove(@RequestParam(value="id") Integer id) {
		ResultModel result = kpiIndexAction.remove(id);
		
		return result.toMap();
	}
	
	/**
	 * 修改
	 * @param record
	 * @param bindingResult
	 * @param model
	 * @return
	 */
	@RequestMapping(value="modify.json")
	public Map<String, Object> modify(KpiIndexVo record, 
			BindingResult bindingResult) {
		//数据绑定错误
		if (bindingResult.hasErrors()) {
			return parseBindingResult(bindingResult);
		}
		ResultModel result = kpiIndexAction.modify(record);
		
		return result.toMap();
	}
}
