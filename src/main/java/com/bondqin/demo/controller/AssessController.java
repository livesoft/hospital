package com.bondqin.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bondqin.demo.dao.EmployeeDao;
import com.bondqin.demo.entity.Employee;

/**
 * 医院年度计划Controller
 * 全部返回json格式数据
 * @author bond
 *
 */
@Controller
public class AssessController extends BaseController {
	
	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="/employeeAssess/index.do")
	public void employeeAssessIndex(Map<String, Object> model) {
		List<Employee> employeeList = employeeDao.findAll();
		
		model.put("employeeList", employeeList);
	}	

	@RequestMapping(value="/directorAssess/index.do")
	public void directorAssessIndex(Map<String, Object> model) {
		Employee emp = getEmployee();
		List<Employee> employeeList = employeeDao.findByDirectLeader(emp);
		
		model.put("employeeList", employeeList);
	}	
}
