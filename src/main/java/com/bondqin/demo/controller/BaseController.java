package com.bondqin.demo.controller;

import java.beans.PropertyEditor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.bondqin.demo.dao.EmployeeDao;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.util.DateUtil;
import com.bondqin.demo.util.MessagesUtil;
import com.bondqin.demo.util.PrincipalUtil;
import com.bondqin.demo.vo.ResultModel;

/**
 * Controller基类
 * @author bond
 *
 */
public abstract class BaseController {
	
	@Autowired
	protected HttpServletRequest request;
	
	@Autowired
	private EmployeeDao employeeDao;
	
	/**
	 * 获取员工信息
	 * @param workNo
	 * @return
	 */
	public Employee getEmployee() {
		String workNo = PrincipalUtil.getUsername();
		return employeeDao.findByWorkNo(workNo);
	}
	
	/**
	 * 获取员工信息
	 * @param workNo
	 * @return
	 */
	public Employee getEmployee(String workNo) {
		return employeeDao.findByWorkNo(workNo);
	}
	
	/**
	 * 日期格式转换
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, getCustomerDateEditor());
	}

	protected PropertyEditor getCustomerDateEditor() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.DATE_PATTERN);
		dateFormat.setLenient(false);
		return new CustomDateEditor(dateFormat, false);
	}
	

	/**
	 * 把BindingResult转换为Map
	 * @param bindingResult
	 * @return
	 */
	protected Map<String, Object> parseBindingResult(BindingResult bindingResult) {
		ResultModel result = new ResultModel();
		
		List<FieldError> fieldErrors = bindingResult.getFieldErrors();
		
		if (fieldErrors != null && fieldErrors.size() > 0) {
			
			for (FieldError error : fieldErrors) {
				String msg = MessagesUtil.getMessage(error.getCodes(), error.getDefaultMessage());
				result.putError(error.getField(), msg);
			}
		}
		return result.toMap();
	}
}
