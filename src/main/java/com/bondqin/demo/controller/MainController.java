package com.bondqin.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.UrlFilenameViewController;

import com.bondqin.demo.dao.EmployeeDao;
import com.bondqin.demo.entity.Department;
import com.bondqin.demo.entity.Employee;
import com.bondqin.demo.util.PrincipalUtil;

/**
 * 访问tiles模板
 * @author bond
 *
 */
@Controller
public class MainController extends UrlFilenameViewController {

	protected Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private EmployeeDao employeeDao;

	/**
	 * 直接访问JSP
	 */
	@RequestMapping(value="/**/*")
	public String viewJsp(HttpServletRequest request) {
		return getViewNameForRequest(request);
	}

	/**
	 * 访问tiles模板
	 */
	@RequestMapping(value="/**/*.htm")
	public String viewLayout(HttpServletRequest request,
			Map<String, Object> model) {
		model.put("body", getViewNameForRequest(request) + ".do");

		//获取员工信息
		String workNo = PrincipalUtil.getUsername();
		Employee employee = employeeDao.findByWorkNo(workNo);
		if (employee != null) {
			Department dept = employee.getDepartment();
			if (dept != null) {
				model.put("deptType", dept.getType());
			}
			model.put("username", PrincipalUtil.getUsername());
			model.put("employeeName", employee.getEmployeeName());
			model.put("roleText", PrincipalUtil.getRoleText());
		}
		
		return "layout";
	}

}
