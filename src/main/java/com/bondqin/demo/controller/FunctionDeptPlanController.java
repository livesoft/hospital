package com.bondqin.demo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bondqin.demo.action.FunctionDeptPlanAction;
import com.bondqin.demo.vo.ResultModel;

/**
 * 医院年度计划Controller
 * 全部返回json格式数据
 * @author bond
 *
 */
@Controller
@RequestMapping(value="/functionDeptPlan")
public class FunctionDeptPlanController extends BaseController {
	
	@Autowired
	private FunctionDeptPlanAction functionDeptPlanAction;

	/**
	 * 分页查询列表
	 * @param number	页码		参数
	 * @param size		每页个数	参数
	 * @param model		模型对象
	 * 
	 */
	@RequestMapping(value="modify.do")
	public void modify(@RequestParam(value="id") Integer id,
			Map<String, Object> model) {
		ResultModel result = functionDeptPlanAction.listOne(id);
		model.putAll(result.toMap());
		
	}	

}
