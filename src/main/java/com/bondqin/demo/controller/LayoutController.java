package com.bondqin.demo.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ����tilesģ��
 * @author bond
 *
 */
@Controller
@RequestMapping(value="layout")
public class LayoutController extends BaseController {

	protected Logger log = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value="menu{index}")	
	public void menu(@PathVariable(value="index") String menu, 
			Map<String, Object> model) {
		log.info(menu);
	}
	
	@RequestMapping(value="header{index}")	
	public void header(@PathVariable(value="index") String header, 
			Map<String, Object> model) {
		
	}
}
