<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
<!--
.typeahead{
  width:100%;

}
-->
</style>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">360评价</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">提交</button>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-10">
                        <form id="form1">
                        	<input type="hidden" name="scoreSource" value="2"/>
                            <div class="form-group">
                                <label>选择同事进行评价</label>
                                <select id="employeeID" name="employeeID" class="form-control">
                                	<c:forEach items="${employeeList }" var="employee">
                                		<option value="<c:out value="${employee.id }"/>"><c:out value="${employee.employeeName }"/>(<c:out value="${employee.workNo }"/>)</option>
                                	</c:forEach>
                                </select>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>适用季度</label>
                                <select id="conductApplyDate" name="assessmentApplyDate" class="form-control">
                               		<option value="201602">201602</option>
                                	<option value="201601">201601</option>
                                	<option value="201504">201504</option>
                                	<option value="201503">201503</option>
                                </select>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
	                            <table class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
			                             <tr>
			                                 <th>指标编号</th>
			                                 <th>指标名称</th>
			                                 <th>指标类别编号</th>
			                                 <th>指标类别</th>
			                                 <th>指标相对权重</th>
			                                 <th>指标备注</th>
			                                 <th>打分</th>
			                             </tr>
			                         </thead>
								</table>
							</div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>

</script>
 
<script>
var ahead;
$(function() {
	$("#conductApplyDate").change(function() {
		$('#dataTables-example').dataTable().fnUpdate();
	});
	$('#dataTables-example').dataTable({
    	"lengthChange" : false,
		"filter": false,
	    "responsive": true,
	    "paginate" : false,
	    "sort" : false,
	    "info" : false,
		"processing": true,
		"serverSide": true,
		"language": {
	    	"url": '<c:url value="/assets/cn.txt"/>'
	    },
		"ajaxSource" : '<c:url value="/conductMethod/listByDate.json"/>',
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({
				name : "applyDate",
				value : $("#conductApplyDate").val()
			},
			{
				name : "employeeID",
				value : $("#employeeID").val()
			});
			$.ajax( {
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			} );
		},
		"columns": [
		  {"data": "id"},
		  {"data": "indexName"},
		  {"data": "indexCategoryID"},
		  {"data": "indexCategoryName"},
		  {"data": "indexComparativeWeight"},
		  {"data": "indexRemark"}
		],
		"columnDefs": [
		  {
		    "targets": [6],
		    "data": "id",
		    "render": function(data, type, full, line) {
		      	return '<input type="hidden" name="kpiAssessList[' + line.row + '].kpiIndex" value="' + data + '"/><input type="text" name="kpiAssessList[' + line.row + '].indexScore" value="10" class="form-control" style="width:50px" />';
		    }
		  }
		]
    });
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/assess/add.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				alert("提交成功");
				window.location.href = '<c:url value="/employeeAssess/index.htm"/>';
			}
		});
	});	
});
$(function() {
	var path = '<c:url value="/employeeAssess/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>