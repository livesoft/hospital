<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">修改评价指标</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
			&nbsp;<a href="<c:url value="/kpiIndex/index.htm"/>"><button type="button" class="btn btn-primary">取消</button></a>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-6">
                        <form id="form1">
                        	<input type="hidden" name="id" value='<c:out value="${data.id }"/>'/>
                            <div class="form-group">
                                <label>指标名称</label>
                                <input name="indexName" value='<c:out value="${data.indexName }"/>' class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>指标类别编号</label>
                                <input name="indexCategoryID" value='<c:out value="${data.indexCategoryID }"/>' class="form-control">
                            </div>
                            <div class="form-group">
                                <label>指标类别名称</label>
                            	<input name="indexCategoryName" value='<c:out value="${data.indexCategoryName }"/>' class="form-control">
                            </div>
                            <div class="form-group">
                                <label>指标相对权重</label>
                            	<input name="indexComparativeWeight" value='<c:out value="${data.indexComparativeWeight }"/>' class="form-control">
                            </div>
                            <div class="form-group">
                                <label>指标备注</label>
                            	<input name="indexRemark" value='<c:out value="${data.indexRemark }"/>' class="form-control">
                            </div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/kpiIndex/modify.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				window.location.href = '<c:url value="/kpiIndex/index.htm"/>';
			}
		});
	});
});
$(function() {
	var path = '<c:url value="/kpiIndex/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>