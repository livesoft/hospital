<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">评价体系</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><a href="<c:url value="/kpiIndex/add.htm"/>"><button type="button" class="btn btn-primary">新增</button></a></div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-example">
						<thead>
                             <tr>
                                 <th>指标编号</th>
                                 <th>指标名称</th>
                                 <th>指标类别编号</th>
                                 <th>指标类别</th>
                                 <th>指标相对权重</th>
                                 <th>指标备注</th>
                                 <th width="95px">操作</th>
                             </tr>
                         </thead>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
	$('#dataTables-example').dataTable({
    	"lengthChange" : false,
		"filter": false,
	    "responsive": true,
	    "sort" : false,
	    "displayLength" : 10,
    	"language": {
	    	"url": '<c:url value="/assets/cn.txt"/>'
	    },
		"processing": true,
		"serverSide": true,
		"ajax" : '<c:url value="/kpiIndex/listAll.json"/>',
		"columns": [
		  {"data": "id"},
		  {"data": "indexName"},
		  {"data": "indexCategoryID"},
		  {"data": "indexCategoryName"},
		  {"data": "indexComparativeWeight"},
		  {"data": "indexRemark"}
		],
		"columnDefs": [
		  {
		    "targets": [6],
		    "data": "id",
		    "render": function(data, type, full) {
		    	var modify = '<a href="<c:url value="/kpiIndex/modify.htm"/>?id=' + data + '"><button type="button" class="btn btn-primary">修改</button></a>';
		    	
		      	return modify;// + ' <button type="button" class="btn btn-primary" onClick="removeData(\'' + data + '\')">删除</button>';
		    }
		  }
		]
    });
});
function removeData(id) {
	if (confirm("确定删除？")) {
		$.ajax({
			url : '<c:url value="/kpiIndex/remove.json"/>',
			dataType : "json",
			type : "post",
			data : {
				id : id
			},
			success : function() {
				$('#dataTables-example').dataTable().fnUpdate();
			}
		});
	}
}
</script>
<script>
$(function() {
	var path = '<c:url value="/kpiIndex/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>