<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">新增职能科室目标</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
			&nbsp;<a href="<c:url value="/functionDeptPlan/index.htm"/>"><button type="button" class="btn btn-primary">取消</button></a>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-6">
                        <form id="form1">
                            <div class="form-group">
                                <label>计划名称</label>
                                <input name="planName" value="2016年职能科室目标 " class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>开始日期</label>
                                <input id="planStartDate" name="planStartDate" value="2016-01-01" class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>结束日期</label>
                                <input id="planStopDate" name="planStopDate" value="2016-12-31" class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>管理目标：</label>
                                <textarea name="manageGoal" value="" class="form-control" rows="3">管理目标完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>文化建设目标：</label>
                                <textarea name="cultureBuildGoal" value="" class="form-control" rows="3">文化建设目标完成。。。</textarea>
                            </div>
                         
                            <div class="form-group">
                                <label>补充内容：</label>
                                <textarea name="addedContent" value="" class="form-control" rows="3">补充。。。</textarea>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$('#planStartDate').datepicker({
        language: 'zh-CN',
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
	$('#planStopDate').datepicker({
        language: 'zh-CN',
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/functionDeptPlan/add.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				window.location.href = '<c:url value="/functionDeptPlan/index.htm"/>';
			}
		});
	});
});
$(function() {
	var path = '<c:url value="/functionDeptPlan/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>