<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">新增述职报告</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
			&nbsp;<a href="<c:url value="/workReport/index.htm"/>"><button type="button" class="btn btn-primary">取消</button></a>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-6">
                        <form id="form1">
                            <div class="form-group">
                                <label>述职报告名称</label>
                                <input value="我的工作表现" name="reportName" class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>述职报告日期</label>
                                <input name="reportDate" value="201602" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>述职报告内容</label>
                                <textarea name="reportContent" class="form-control" rows="3">我的工作内容</textarea>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/workReport/add.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				window.location.href = '<c:url value="/workReport/index.htm"/>';
			}
		});
	});
});
$(function() {
	var path = '<c:url value="/workReport/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>