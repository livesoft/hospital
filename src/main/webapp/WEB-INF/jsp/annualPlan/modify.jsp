<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">修改医院目标</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
			&nbsp;<a href="<c:url value="/annualPlan/index.htm"/>"><button type="button" class="btn btn-primary">取消</button></a>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-6">
                        <form id="form1">
                        	<input type="hidden" name="planID" value='<c:out value="${data.planID }"/>'/>
                            <div class="form-group">
                                <label>计划名称</label>
                                <input name="planName" class="form-control" value='<c:out value="${data.planName }"/>'>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>适用年份</label>
                                <input name="planMeasure" value='<c:out value="${data.planMeasure }"/>' class="form-control">
                            </div>
                            <div class="form-group">
                                <label>业务目标：</label>
                                <textarea name="transactionGoal" class="form-control" rows="3"><c:out value="${data.transactionGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>科研目标：</label>
                                <textarea name="projectGoal" class="form-control" rows="3"><c:out value="${data.projectGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>管理目标：</label>
                                <textarea name="manageGoal" class="form-control" rows="3"><c:out value="${data.manageGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>财务目标：</label>
                                <textarea name="financeGoal" class="form-control" rows="3"><c:out value="${data.financeGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>人才建设目标：</label>
                                <textarea name="talentBuildGoal" class="form-control" rows="3"><c:out value="${data.talentBuildGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>硬件建设目标：</label>
                                <textarea name="hardwareBuildGoal" class="form-control" rows="3"><c:out value="${data.hardwareBuildGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>文化建设目标：</label>
                                <textarea name="cultureBuildGoal" class="form-control" rows="3"><c:out value="${data.cultureBuildGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>补充内容：</label>
                                <textarea name="addedContent" class="form-control" rows="3"><c:out value="${data.addedContent }"/></textarea>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/annualPlan/modify.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				window.location.href = '<c:url value="/annualPlan/index.htm"/>';
			}
		});
	});
});
$(function() {
	var path = '<c:url value="/annualPlan/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>