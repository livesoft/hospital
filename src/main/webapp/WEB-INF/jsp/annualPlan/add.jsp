<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">新增医院目标</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
			&nbsp;<a href="<c:url value="/annualPlan/index.htm"/>"><button type="button" class="btn btn-primary">取消</button></a>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-6">
                        <form id="form1">
                            <div class="form-group">
                                <label>计划名称</label>
                                <input name="planName" value="2016年医院目标" class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>适用年份</label>
                                <select name="conductApplyDate" class="form-control">
                               		<option value="2016">2016</option>
                                	<option value="2017">2017</option>
                                	<option value="2018">2018</option>
                                	<option value="2019">2019</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>业务目标：</label>
                                <textarea name="transactionGoal" value="" class="form-control" rows="3">业务完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>科研目标：</label>
                                <textarea name="projectGoal" value="" class="form-control" rows="3">科研完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>管理目标：</label>
                                <textarea name="manageGoal" value="" class="form-control" rows="3">管理完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>财务目标：</label>
                                <textarea name="financeGoal" value="" class="form-control" rows="3">财务完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>人才建设目标：</label>
                                <textarea name="talentBuildGoal" value=""  class="form-control" rows="3">人才建设完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>硬件建设目标：</label>
                                <textarea name="hardwareBuildGoal" value="" class="form-control" rows="3">硬件建设完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>文化建设目标：</label>
                                <textarea name="cultureBuildGoal" value="" class="form-control" rows="3">文化建设完成。。。</textarea>
                            </div>
                            <div class="form-group">
                                <label>补充内容：</label>
                                <textarea name="addedContent" value="" class="form-control" rows="3">补充。。。</textarea>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/annualPlan/add.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				window.location.href = '<c:url value="/annualPlan/index.htm"/>';
			}
		});
	});
});
$(function() {
	var path = '<c:url value="/annualPlan/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>