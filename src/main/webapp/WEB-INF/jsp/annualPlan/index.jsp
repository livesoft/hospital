<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">医院目标</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><a href="<c:url value="/annualPlan/add.htm"/>"><button type="button" class="btn btn-primary">新增</button></a></div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-bordered table-hover"
						id="dataTables-example">
						<thead>
                             <tr>
                                 <th>计划名称</th>
                                 <th>计划编号</th>
                                 <th>适用年份</th>
                                 <th width="95px">操作</th>
                             </tr>
                         </thead>
					</table>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
$(document).ready(function() {
    $('#dataTables-example').dataTable({
    	"lengthChange" : false,
		"filter": false,
	    "responsive": true,
	    "sort" : false,
	    "displayLength" : 10,
    	"language": {
	    	"url": '<c:url value="/assets/cn.txt"/>'
	    },
		"processing": true,
		"serverSide": true,
		"ajax" : '<c:url value="/annualPlan/listAll.json"/>',
		"columns": [
		  {"data": "planName"},
		  {"data": "planID"},
		  {"data": "planMeasure"}
		],
		"columnDefs": [
		  {
		    "targets": [3],
		    "data": "planID",
		    "render": function(data, type, full) {
		    	var modify = '<a href="<c:url value="/annualPlan/modify.htm"/>?id=' + data + '"><button type="button" class="btn btn-primary">修改</button></a>';
		    	
		      	return modify + ' <button type="button" class="btn btn-primary" onClick="removeData(\'' + data + '\')">删除</button>';
		    }
		  }
		]
    });
});
function removeData(id) {
	if (confirm("确定删除？")) {
		$.ajax({
			url : '<c:url value="/annualPlan/remove.json"/>',
			dataType : "json",
			type : "post",
			data : {
				id : id
			},
			success : function() {
				$('#dataTables-example').dataTable().fnUpdate();
			}
		});
	}
}
</script>
<script>
$(function() {
	var path = '<c:url value="/annualPlan/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>