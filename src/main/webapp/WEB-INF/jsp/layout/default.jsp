<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href='<c:url value="/assets/bower_components/bootstrap/dist/css/bootstrap.min.css"/>' rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href='<c:url value="/assets/bower_components/metisMenu/dist/metisMenu.min.css"/>' rel="stylesheet">

	<!-- DataTables CSS -->
    <link href='<c:url value="/assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css"/>' rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href='<c:url value="/assets/bower_components/datatables-responsive/css/responsive.bootstrap.css"/>' rel="stylesheet">

	<!-- Datepicker CSS -->
    <link href='<c:url value="/assets/css/bootstrap-datepicker.css"/>' rel="stylesheet">

    <!-- Custom CSS -->
    <link href='<c:url value="/assets/dist/css/sb-admin-2.css"/>' rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='<c:url value="/assets/bower_components/font-awesome/css/font-awesome.min.css"/>' rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- jQuery -->
    <script src='<c:url value="/assets/bower_components/jquery/dist/jquery.min.js"/>'></script>
    
    <script src="http://v2.bootcss.com/assets/js/bootstrap-typeahead.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src='<c:url value="/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"/>'></script>

    <script src='<c:url value="/assets/js/bootstrap-datepicker.js"/>'></script>
	<script src='<c:url value="/assets/js/bootstrap-datepicker.zh-CN.js"/>'></script>
	
    <!-- Metis Menu Plugin JavaScript -->
    <script src='<c:url value="/assets/bower_components/metisMenu/dist/metisMenu.min.js"/>'></script>

	<!-- DataTables JavaScript -->
	<script src='<c:url value="/assets/bower_components/datatables/media/js/jquery.dataTables.min.js"/>'></script>
	<script src='<c:url value="/assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"/>'></script>
	
    <!-- Custom Theme JavaScript -->
    <script src='<c:url value="/assets/dist/js/sb-admin-2.js"/>'></script>
	
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href='<c:url value="/index.htm"/>'>XX公立医院绩效管理系统</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <c:out value="${username }"/> <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li> -->
                        <li><a href="#">&nbsp; <c:out value="${employeeName }"/>&nbsp;&nbsp;(<c:out value="${roleText }"/>)</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href='<c:url value="/doLogout"/>'s><i class="fa fa-sign-out fa-fw"></i> 退出</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <li>
                            <a href='<c:url value="/index.htm"/>'><i class="fa fa-dashboard fa-fw"></i> 主页</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> 目标设定<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                           		<!-- 院办权限 -->
                                <sec:authorize access ="hasRole('ROLE_R1')" >
                                <li>
                                    <a href='<c:url value="/annualPlan/index.htm"/>'>医院目标</a>
                                </li>
                                </sec:authorize>
                                <!-- 院办,科长权限 -->
                                <sec:authorize access ="hasAnyRole('ROLE_R3')" >
                                <!-- 业务科室权限 -->
                                <c:choose>
                                <c:when test="${deptType == '1' }">
                                <li>
                                    <a href='<c:url value="/serviceDeptPlan/index.htm"/>'>业务科室目标</a>
                                </li>
                                </c:when>
                                <c:when test="${deptType == '2' }">
                                <li>
                                    <a href='<c:url value="/functionDeptPlan/index.htm"/>'>职能科室目标</a>
                                </li>
                                </c:when>
                                <c:otherwise>
                                <li>
                                    <a href='<c:url value="/serviceDeptPlan/index.htm"/>'>业务科室目标</a>
                                </li>
                                <li>
                                    <a href='<c:url value="/functionDeptPlan/index.htm"/>'>职能科室目标</a>
                                </li>
                                </c:otherwise>
                                </c:choose>
                                </sec:authorize>
                                <!-- 员工权限 -->
                                <sec:authorize access ="hasRole('ROLE_R5')" >
                                <li>
                                    <a href='<c:url value="/pesonalPlan/index.htm"/>'>个人目标</a>
                                </li>
                                </sec:authorize>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!-- 员工权限 -->
                        <%-- <sec:authorize access ="hasRole('ROLE_R5')" >
                        <li>
                            <a href='<c:url value="/reviewReference/index.htm"/>'><i class="fa fa-table fa-fw"></i> 记录日常工作</a>
                        </li>
                        </sec:authorize> --%>
                        <sec:authorize access ="hasAnyRole('ROLE_R1,ROLE_R2')" >
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> 准备绩效考核<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<c:url value="/kpiIndex/index.htm"/>">设计评价体系</a>
                                </li>
                                <li>
                                    <a href='<c:url value="/conductMethod/index.htm"/>'>制定考核流程</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        </sec:authorize>
                        <li >
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> 绩效考核<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                            	<sec:authorize access ="hasAnyRole('ROLE_R5')" >
                                <li>
                                    <a href="<c:url value="/workReport/index.htm"/>">述职报告</a>
                                </li>
                                </sec:authorize>
                                <sec:authorize access ="hasAnyRole('ROLE_R4')" >
                                <li>
                                    <a href="<c:url value="/directorAssess/index.htm"/>">领导评价</a>
                                </li>
                                </sec:authorize>
                                <sec:authorize access ="hasAnyRole('ROLE_R5')" >
                                <li>
                                    <a href="<c:url value="/employeeAssess/index.htm"/>">360评价</a>
                                </li>
                                </sec:authorize>
                                <sec:authorize access ="hasAnyRole('ROLE_R2,ROLE_R4,ROLE_R5')" >
                                <li>
                                    <a href="<c:url value="/checkResult/index.htm"/>">考核结果</a>
                                </li>
                                </sec:authorize>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <!-- <li >
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> 绩效反馈<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">分析考核结果</a>
                                </li>
                                <li>
                                    <a href="#">奖惩安排 <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level  ">
                                        <li>
                                            <a href="#"  >薪资调整计划</a>
                                        </li>
                                        <li>
                                            <a href="#">职位变动计划</a>
                                        </li>
                                        <li>
                                            <a href="#">员工培训计划</a>
                                        </li>
                                        <li>
                                            <a href="#" >招聘计划</a>
                                        </li>
                                        <li>
                                            <a href="#" >申述</a>
                                        </li>
                                    </ul>
                                    /.nav-third-level
                                </li>
                            </ul>
                            /.nav-second-level
                        </li> -->
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
        	<tiles:insertAttribute name="body" />
            <!-- <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Blanxk</h1>
                    </div>
                    /.col-lg-12
                </div>
                /.row
            </div> -->
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>

</html>
