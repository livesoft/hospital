<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8"%>
<html>
<head>
</head>
<body>
<div style="position: fixed;height: 100%;width: 100%;z-index: -1;">
	<img style="height: 100%;width: 100%;" src='<c:url value="/assets/images/error.jpg" />'/>
</div>
<div style="display:none">
<c:out value="${exception.message }"/>
</div>
</body>
</html>
