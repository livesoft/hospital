<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
<!--
.typeahead{
  width:100%;

}
-->
</style>
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">考核结果</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">申述</button>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-10">
                        <form id="form1">
                            <div class="form-group">
                                <label>适用季度</label>
                                <select id="conductApplyDate" name="conductApplyDate" class="form-control">
                               		<option value="201602">201602</option>
                                	<option value="201601">201601</option>
                                	<option value="201504">201504</option>
                                	<option value="201503">201503</option>
                                </select>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>总体得分</label>
                                <input value="" id="totalScore" name="" style="width:120px" class="form-control">
                            </div>
                            <div class="form-group">
	                            <table class="table table-striped table-bordered table-hover"
									id="dataTables-example">
									<thead>
			                             <tr>
			                                 <th>指标编号</th>
			                                 <th>指标名称</th>
			                                 <th>指标类别编号</th>
			                                 <th>指标类别</th>
			                                 <th>指标相对权重</th>
			                                 <th>指标备注</th>
			                                 <th>得分</th>
			                             </tr>
			                         </thead>
								</table>
							</div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>

</script>
 
<script>
var ahead;
$(function() {
	$("#conductApplyDate").change(function() {
		$('#dataTables-example').dataTable().fnUpdate();
	});
	$('#dataTables-example').dataTable({
    	"lengthChange" : false,
		"filter": false,
	    "responsive": true,
	    "paginate" : false,
	    "sort" : false,
	    "info" : false,
		"processing": true,
		"serverSide": true,
		"language": {
	    	"url": '<c:url value="/assets/cn.txt"/>'
	    },
		"ajaxSource" : '<c:url value="/conductMethod/listByDateMyselfScore.json"/>',
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({
				name : "applyDate",
				value : $("#conductApplyDate").val()
			});
			$.ajax( {
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": function(data) {
					$("#totalScore").val(data.totalScore);
					
					fnCallback(data);
				}
			} );
		},
		"columns": [
		  {"data": "id"},
		  {"data": "indexName"},
		  {"data": "indexCategoryID"},
		  {"data": "indexCategoryName"},
		  {"data": "indexComparativeWeight"},
		  {"data": "indexRemark"}
		],
		"columnDefs": [
		  {
		    "targets": [6],
		    "data": "score",
		    "render": function(data, type, full, line) {
		      	return data;
		    }
		  }
		]
    });
	$("#saveBtn").click(function() {
		alert("提交成功");
	});	
});
$(function() {
	var path = '<c:url value="/checkResult/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>