<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">修改个人目标</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
			&nbsp;<a href="<c:url value="/pesonalPlan/index.htm"/>"><button type="button" class="btn btn-primary">取消</button></a>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-6">
                        <form id="form1">
                    	    <input type="hidden" name="id" value='<c:out value="${data.id }"/>'/>
                            <div class="form-group">
                                <label>计划名称</label>
                                <input name="planName" value="<c:out value="${data.planName }"/>" class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>开始日期</label>
                                <input id="planStartDate" value="<c:out value="${data.planStartDate }"/>" name="planStartDate" value="2016-01-01" class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>结束日期</label>
                                <input id="planStopDate" value="<c:out value="${data.planStopDate }"/>" name="planStopDate" value="2016-12-31" class="form-control">
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                              <div class="form-group">
                                <label>业务目标：</label>
                                <textarea name="transactionGoal" value="" class="form-control" rows="3"><c:out value="${data.transactionGoal }"/></textarea>
                            </div>
                            <div class="form-group">
                                <label>科研目标：</label>
                                <textarea name="projectGoal" value="" class="form-control" rows="3"><c:out value="${data.projectGoal }"/></textarea>
                            </div>
                          
                       
                            <div class="form-group">
                                <label>补充内容：</label>
                                <textarea name="addedContent" value="" class="form-control" rows="3"><c:out value="${data.addedContent }"/></textarea>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$('#planStartDate').datepicker({
        language: 'zh-CN',
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
	$('#planStopDate').datepicker({
        language: 'zh-CN',
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/pesonalPlan/modify.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				window.location.href = '<c:url value="/pesonalPlan/index.htm"/>';
			}
		});
	});
});
$(function() {
	var path = '<c:url value="/pesonalPlan/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>