<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href='<c:url value="/assets/bower_components/bootstrap/dist/css/bootstrap.min.css"/>' rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href='<c:url value="/assets/bower_components/metisMenu/dist/metisMenu.min.css"/>' rel="stylesheet">

    <!-- Custom CSS -->
    <link href='<c:url value="/assets/dist/css/sb-admin-2.css"/>' rel="stylesheet">

    <!-- Custom Fonts -->
    <link href='<c:url value="/assets/bower_components/font-awesome/css/font-awesome.min.css"/>' rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<form action="doLogin" name="form1" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">XX公立医院绩效管理系统</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="用户名" name="username" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="密码" name="password" type="password" value="">
                                </div>
                                <div class="form-group">
                                <c:if test="${param.error != null}">
									<font color="red"><c:out value="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message}"/></font>
								</c:if>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="myAppKey" checked type="checkbox" value="Remember Me">记住我
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a href="javascript:;" onClick="form1.submit()" class="btn btn-lg btn-success btn-block">登录</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src='<c:url value="/assets/bower_components/jquery/dist/jquery.min.js"/>'></script>

    <!-- Bootstrap Core JavaScript -->
    <script src='<c:url value="/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"/>'></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src='<c:url value="/assets/bower_components/metisMenu/dist/metisMenu.min.js"/>'></script>

    <!-- Custom Theme JavaScript -->
    <script src='<c:url value="/assets/dist/js/sb-admin-2.js"/>'></script>
</form>
</body>

</html>
