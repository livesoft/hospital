<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">修改考核流程</h3>
	</div>
	
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
			<button type="button" class="btn btn-primary" id="saveBtn">保存</button>
			&nbsp;<a href="<c:url value="/conductMethod/index.htm"/>"><button type="button" class="btn btn-primary">取消</button></a>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="row">
                    <div class="col-lg-6">
                        <form id="form1">
                        	<input type="hidden" name="rankMethodID" value="1"/>
                        	<input type="hidden" name="id" value="<c:out value="${data.id }"/>"/>
                            <div class="form-group">
                                <label>适用季度</label>
                                <select name="conductApplyDate" class="form-control">
                               		<option value="201602" <c:if test="${data.conductApplyDate == '201602' }">selected</c:if>>201602</option>
                                	<option value="201601" <c:if test="${data.conductApplyDate == '201601' }">selected</c:if>>201601</option>
                                	<option value="201504" <c:if test="${data.conductApplyDate == '201504' }">selected</c:if>>201504</option>
                                	<option value="201503" <c:if test="${data.conductApplyDate == '201503' }">selected</c:if>>201503</option>
                                </select>
                                <!-- <p class="help-block">Example block-level help text here.</p> -->
                            </div>
                            <div class="form-group">
                                <label>名称</label>
                                <input name="conductName" value='<c:out value="${data.conductName }"/>' class="form-control">
                            </div>
                            <div class="form-group">
                           		<label>领导评价占比</label>
                           		<div class="form-group input-group">
                                    <input name="managerAssessmentRate" value='<c:out value="${data.managerAssessmentRate }"/>' type="text" class="form-control">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>员工评价占比</label>
                                <div class="form-group input-group">
                                    <input name="employeeAssessmentRate" value='<c:out value="${data.employeeAssessmentRate }"/>' type="text" class="form-control">
                                    <span class="input-group-addon">%</span>
                                </div>
	                        </div>
	                        <div class="form-group">
	                        	<label>业务部门采用的指标类别</label>
		                        <c:forEach items="${kpiIndex }" var="kpi" varStatus="status">
			                        <div class="checkbox">
	                                	<label>
		                                    <input type="checkbox" checked name="serviceKpis[<c:out value="${status.index }"/>].kpiIndex" value="<c:out value="${kpi.id }"/>"><c:out value="${kpi.indexName }"/>
		                                </label>
		                                <div class="form-group input-group">
		                              	    <input name="serviceKpis[<c:out value="${status.index }"/>].type" value="1" type="hidden">
		                                    <input name="serviceKpis[<c:out value="${status.index }"/>].indexCategoryWeight" value="40" type="text" class="form-control">
		                                    <span class="input-group-addon">%</span>
		                                </div>
	                                </div>
		                        </c:forEach>
                            </div>
	                        <div class="form-group">
	                        	<label>职能部门采用的指标类别</label>
		                        <c:forEach items="${kpiIndex }" var="kpi" varStatus="status">
			                        <div class="checkbox">
	                                	<label>
		                                    <input type="checkbox" checked name="functionKpis[<c:out value="${status.index }"/>].kpiIndex" value="<c:out value="${kpi.id }"/>"><c:out value="${kpi.indexName }"/>
		                                </label>
		                                <div class="form-group input-group">
		                                	<input name="functionKpis[<c:out value="${status.index }"/>].type" value="2" type="hidden">
		                                    <input name="functionKpis[<c:out value="${status.index }"/>].indexCategoryWeight" value="40" type="text" class="form-control">
		                                    <span class="input-group-addon">%</span>
		                                </div>
	                                </div>
		                        </c:forEach>
                            </div>
                            <div class="form-group">
                                <label>补充内容</label>
                                <textarea name="addedContent"  class="form-control" rows="3"><c:out value="${data.addedContent }"/></textarea>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-lg-6 (nested) -->
                    
                    <!-- /.col-lg-6 (nested) -->
                </div>
			</div>
		</div>
	</div>
</div>
<script>
$(function() {
	$("input[type=checkbox]").click(function() {
		if ($(this).prop("checked")) {
			$(this).parent().parent().find("input").prop("disabled", false);
		} else {
			$(this).parent().parent().find("input").prop("disabled", true);
		}
	});
	$("#saveBtn").click(function() {
		$.ajax({
			url : '<c:url value="/conductMethod/modify.json"/>',
			dataType : "json",
			type : "post",
			data : $("#form1").serialize(),
			success : function() {
				window.location.href = '<c:url value="/conductMethod/index.htm"/>';
			}
		});
	});
});
$(function() {
	var path = '<c:url value="/conductMethod/index.htm"/>';
	$(".navbar-default a").each(function(i, e) {
		var href = $(this).attr("href");
		if (href.indexOf(path) == 0) {
			$(this).addClass("active").parents("ul").addClass("collapse").addClass("in")
			.parents("li").addClass("active");
		}
	});
});
</script>